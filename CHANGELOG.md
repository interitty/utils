# Changelog #
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] ##

### Added ###

### Changed ###

### Fixed ###

## [1.0.21] - 2024-12-08 ##

### Changed ###

- PHP 8.3 property static types
- Upgrade dependent packages
- Upgrade license to 2025

## [1.0.20] - 2024-08-30 ##

### Changed ###

- Remove deprecated Interitty\Utils\Mapper envelope
- Upgrade dependent packages

### Fixed ###

- Phpstan extension.neon of composer/pcre missing by dg/composer-cleaner

## [1.0.19] - 2024-08-16 ##

### Changed ###

- Array topological sort moved to the ArraySorter class
- Array topological sort now support also ArrayAccess&Traversable
- Array sort ascending by nested value
- Array sort descending by nested value

### Fixed ###

- PHPUnit code coverage exclude PHPStan extension
- Typo in CHANGELOG

## [1.0.18] - 2024-06-27 ##

### Added ###

- Arrays get deep
- Array column
- Array wrapper

## [1.0.17] - 2024-05-19 ##

### Changed ###

- Update SECURITY key
- Upgrade dependent packages

## [1.0.16] - 2024-05-02 ##

### Added ###

- FilterVariable support

## [1.0.15] - 2024-04-22 ##

### Fixed ###

- Fix typo in README.md

## [1.0.14] - 2023-12-28 ##

### Changed ###

- Increase minimal PHP to 8.3
- Update security contacts
- Upgrade dependent packages
- Upgrade license to 2024

## [1.0.13] - 2023-11-11 ##

### Added ###

- Reflection methods attributes

## [1.0.12] - 2023-09-04 ##

### Added ###

- Class or object of type checker
- Class uses getter
- Isolated require (class autoload) processor
- Trait class used by class or object checker
- Trigger event with possible referenced parameters processor

## [1.0.11] - 2023-07-29 ##

### Added ###

- Array to generator helper

## [1.0.10] - 2023-07-29 ##

### Added ###

- Array topological sort

## [1.0.9] - 2023-05-25 ##

### Added ###

- ReflectionObject non-public property getter/setter support
- ReflectionObject parents non-public property support

### Changed ###

- Extended exceptions improve compatibility with handlers

## [1.0.8] - 2023-03-12 ##

### Changed ###

- Update dependencies to newer version

### Fixed ###

- Minor changes due to new version of code checker

## [1.0.7] - 2023-03-10 ##

### Added ###

- Filesystem features

## [1.0.6] - 2022-12-29 ##

### Changed ###

- Upgrade license to 2023
- Increase minimal PHP to 8.2
- Upgrade dependent packages

### Fixed ###

- Missing `@throws Exception` fixed

## [1.0.5] - 2022-10-04 ##

### Changed ###

- Removing call of an unnecessary PhpUnit extension calls (egg and chicken problem)
- Update phpunit dependency to newer version

## [1.0.4] - 2022-10-04 ##

### Added ###

- Extended and translatable exceptions

## [1.0.3] - 2022-09-15 ##

### Changed ###

- Update dependencies to newer version

### Fixed ###

- PHP 8.1 strict return type fix

## [1.0.2] - 2022-07-22 ##

### Added ###

- Collection
- DateTimeFactory
- Readable/Writable/Executable validator

## [1.0.1] - 2022-07-20 ##

### Added ###

- Assertable checks

## [1.0.0] - 2022-07-20 ##

### Added ###

- Basic package
