<?php

declare(strict_types=1);

namespace Interitty\Exceptions;

use Interitty\PhpUnit\BaseTestCase;
use Nette\Localization\Translator;
use Nette\Utils\AssertionException;
use PHPUnit\Framework\MockObject\MockObject;
use RuntimeException;
use Throwable;

use function sys_get_temp_dir;

/**
 * @coversDefaultClass Interitty\Exceptions\Exceptions
 */
class ExceptionsTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of extend for exception implementation
     *
     * @group negative
     * @covers ::extend
     */
    public function testExtendException(): void
    {
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('Given exception should be Throwable object or class name');
        Exceptions::extend('foo'); // @phpstan-ignore-line
    }

    /**
     * Tester of thrown exception additional data availability implementation
     *
     * @return void
     */
    public function testExtendedExceptionThrown(): void
    {
        $class = RuntimeException::class;
        $data = ['foo' => 'bar'];
        $message = 'Message :foo';
        $this->expectExceptionCallback(function (ExtendedExceptionInterface $exception) use ($class, $message): void {
            $this->expectException($class);
            $this->expectExceptionMessage($message);
            self::assertSame('Message bar', (string) $exception);
        });
        $this->expectExceptionData($data);
        throw Exceptions::extend($class)
                ->setMessage($message)
                ->setData($data);
    }

    /**
     * Tester of extended exception for fluent interface implementation: void
     *
     * @return void
     * @group integration
     */
    public function testExtendFluent(): void
    {
        Exceptions::setTempDir($this->createTempDirectory());

        $message = 'Message';
        $code = 42;
        $previous = $this->createThrowableMock();

        $exception = Exceptions::extend(RuntimeException::class);
        self::assertSame($exception, $exception->setMessage($message));
        self::assertSame($message, $exception->getMessage());
        self::assertSame($exception, $exception->addMessage($message));
        self::assertSame($message . $message, $exception->getMessage());
        self::assertSame($exception, $exception->setCode($code));
        self::assertSame($code, $exception->getCode());
        self::assertSame($exception, $exception->setPrevious($previous));
        self::assertSame($previous, $exception->getPrevious());
    }

    /**
     * Tester of extend for given object implementation
     *
     * @group integration
     * @covers ::extend
     * @covers ::createExceptionFactory
     * @covers \Interitty\Exceptions\ExtendedExceptionTrait::addData
     * @covers \Interitty\Exceptions\ExtendedExceptionTrait::getData
     */
    public function testExtendObject(): void
    {
        Exceptions::setTempDir($this->createTempDirectory());

        $message = 'Message with :placeholder';
        $key = 'placeholder';
        $value = 'value';

        $exception = Exceptions::extend(new RuntimeException($message))
            ->addData($key, $value);

        self::assertInstanceOf(RuntimeException::class, $exception); // @phpstan-ignore-line
        self::assertSame([$key => $value], $exception->getData());
        self::assertSame($message, $exception->getMessage());
        self::assertSame('Message with value', (string) $exception);
    }

    /**
     * Tester of extend for given string implementation
     *
     * @return void
     * @group integration
     * @covers ::extend
     * @covers ::createExceptionFactory
     * @covers \Interitty\Exceptions\ExtendedExceptionTrait::__toString
     * @covers \Interitty\Exceptions\ExtendedExceptionTrait::toString
     * @covers \Interitty\Exceptions\ExtendedExceptionTrait::getData
     * @covers \Interitty\Exceptions\ExtendedExceptionTrait::setData
     */
    public function testExtendString(): void
    {
        Exceptions::setTempDir($this->createTempDirectory());

        $message = 'Message with :placeholder';
        $data = ['placeholder' => 'value'];

        $exception = Exceptions::extend(RuntimeException::class)
            ->setMessage($message)
            ->setData($data);

        self::assertInstanceOf(RuntimeException::class, $exception); // @phpstan-ignore-line
        self::assertSame($data, $exception->getData());
        self::assertSame($message, $exception->getMessage());
        self::assertSame('Message with value', (string) $exception);
        self::assertSame('Message with value', $exception->toString());
    }

    /**
     * Tester of extend for setup and use Translator implementation
     *
     * @return void
     * @group integration
     * @covers ::extend
     * @covers ::createExceptionFactory
     * @covers \Interitty\Exceptions\ExtendedExceptionTrait::__toString
     * @covers \Interitty\Exceptions\ExtendedExceptionTrait::getTranslator
     * @covers \Interitty\Exceptions\ExtendedExceptionTrait::setTranslator
     */
    public function testExtendTranslate(): void
    {
        Exceptions::setTempDir($this->createTempDirectory());

        $message = 'Message with :placeholder';
        $data = ['placeholder' => 'value'];
        $translator = $this->createTranslatorMock(['translate']);
        $translator->expects(self::any())
            ->method('translate')
            ->with(self::equalTo($message))
            ->willReturn('Translated message with :placeholder');

        Exceptions::setTranslator($translator);
        $exception = Exceptions::extend(RuntimeException::class)
            ->setMessage($message)
            ->setData($data);

        self::assertSame($data, $exception->getData());
        self::assertSame($message, $exception->getMessageTemplate());
        self::assertSame('Translated message with value', $exception->getMessage());
        self::assertSame('Translated message with value', (string) $exception);
    }

    /**
     * Tester of temp dir getter/setter implementation
     *
     * @return void
     * @covers ::getTempDir
     * @covers ::setTempDir
     */
    public function testGetSetTempDir(): void
    {
        $defaultTempDir = sys_get_temp_dir();
        $tempDir = $this->createTempDirectory();
        $exceptions = $this->createExceptionsMock();
        $this->setNonPublicPropertyValue($exceptions, 'tempDir', null);
        self::assertSame($defaultTempDir, $exceptions::getTempDir());
        $exceptions::setTempDir($tempDir);
        self::assertSame($tempDir, $exceptions::getTempDir());
    }

    /**
     * Tester of translator getter/setter implementation
     *
     * @return void
     * @covers ::getTranslator
     * @covers ::setTranslator
     */
    public function testGetSetTranslator(): void
    {
        $translator = $this->createTranslatorMock();
        $exceptions = $this->createExceptionsMock();
        $exceptions::setTranslator($translator);
        self::assertSame($translator, $exceptions::getTranslator());
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Exceptions mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Exceptions|MockObject
     */
    protected function createExceptionsMock(array $methods = []): MockObject
    {
        $mock = $this->createPartialMock(Exceptions::class, $methods);
        $this->setNonPublicPropertyValue($mock, 'tempDir', null);
        return $mock;
    }

    /**
     * Throwable mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Throwable|MockObject
     */
    protected function createThrowableMock(array $methods = []): MockObject
    {
        $mock = $this->createMockAbstract(Throwable::class, $methods);
        return $mock;
    }

    /**
     * Translator mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Translator|MockObject
     */
    protected function createTranslatorMock(array $methods = []): MockObject
    {
        $mock = $this->createMockAbstract(Translator::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
