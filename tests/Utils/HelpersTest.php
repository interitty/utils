<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Generator;
use Interitty\PhpUnit\BaseTestCase;
use Nette\SmartObject;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use RuntimeException;
use stdClass;

use function class_exists;
use function class_uses;
use function in_array;
use function sprintf;

use const JSON_PRETTY_PRINT;

/**
 * @coversDefaultClass Interitty\Utils\Helpers
 */
class HelpersTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of Isolated require (class autoload) processor implementation
     *
     * @return void
     * @group integration
     * @covers ::processIsolatedRequire
     */
    public function testProcessIsolatedRequire(): void
    {
        $code = '<?php class %s {};';
        /**
         * @phpstan-var class-string $className
         * @phpstan-ignore varTag.nativeType
         */
        $className = 'NewNotAutoloadedClass';
        $classFilePath = $this->createTempFile(sprintf($code, $className));
        self::assertFalse(class_exists($className));
        Helpers::processIsolatedRequire($className, $classFilePath);
        self::assertTrue(class_exists($className));
        Helpers::processIsolatedRequire($className, $classFilePath);
    }

    /**
     * Tester of Isolated require (class autoload) processor for wrong file implementation
     *
     * @return void
     * @group negative
     * @covers ::processIsolatedRequire
     */
    public function testProcessIsolatedRequireWrongFile(): void
    {
        /**
         * @phpstan-var class-string $className
         * @phpstan-ignore varTag.nativeType
         */
        $className = 'AnotherNotAutoloadedClass';
        $wrongFilePath = $this->createTempFile();
        self::assertFalse(class_exists($className));
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Class not found');
        Helpers::processIsolatedRequire($className, $wrongFilePath);
    }

    /**
     * Tester of Trait class used by class or object checker implementation
     *
     * @return void
     * @group integration
     * @covers ::getClassUses
     * @covers ::isTraitUsed
     * @covers ::isTypeOf
     */
    public function testIsTypeOf(): void
    {
        /**
         * @phpstan-var class-string $deepClass
         * @phpstan-ignore varTag.nativeType
         */
        $deepClass = 'BaseClassUsingTrait';
        $deepCode = '<?php class %s {
            use ' . SmartObject::class . ';
        };';
        $this->processRegisterAutoload($deepClass, sprintf($deepCode, $deepClass));

        /**
         * @phpstan-var class-string $class
         * @phpstan-ignore varTag.nativeType
         */
        $class = 'ClassUsingTrait';
        $code = '<?php class %s extends %s {};';
        $this->processRegisterAutoload($class, sprintf($code, $class, $deepClass));

        self::assertFalse(in_array(SmartObject::class, class_uses($class), true));
        self::assertTrue(Helpers::isTraitUsed($class, SmartObject::class));
        self::assertTrue(Helpers::isTypeOf($class, [stdClass::class, $deepClass]));
    }

    /**
     * Data provider wfor testSetObjectVars test
     *
     * @return Generator<string, array{0: object, 1: object|mixed[], 2: object}>
     */
    public function setObjectVarsProvider(): Generator
    {
        $object = (object) ['one' => 'two'];
        // <editor-fold defaultstate="collapsed" desc="array">
        yield 'array' => [$object, ['three' => 'four'], (object) ['one' => 'two', 'three' => 'four']];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="object">
        yield 'object' => [$object, (object) ['three' => 'four'], (object) ['one' => 'two', 'three' => 'four']];
        // </editor-fold>
    }

    /**
     * Tester of Multiple properties to the given object setter implementation
     *
     * @param object $object
     * @param object|mixed[] $vars
     * @param object $expectedObject
     * @return void
     * @covers ::setObjectVars
     * @group integration
     * @dataProvider setObjectVarsProvider
     * @throws JsonException
     */
    public function testSetObjectVars(object $object, $vars, object $expectedObject): void
    {
        Helpers::setObjectVars($object, $vars);
        $result = Json::encode($expectedObject, JSON_PRETTY_PRINT);
        self::assertSame(Json::encode($object, JSON_PRETTY_PRINT), $result);
    }

    // </editor-fold>
}
