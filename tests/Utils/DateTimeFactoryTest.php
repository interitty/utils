<?php

declare(strict_types=1);

namespace Interitty\Utils;

use DateTimeZone;
use Generator;
use Interitty\PhpUnit\BaseTestCase;
use InvalidArgumentException;
use PHPUnit\Framework\MockObject\MockObject;
use Throwable;

use function date_default_timezone_get;

/**
 * @coversDefaultClass Interitty\Utils\DateTimeFactory
 */
class DateTimeFactoryTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of create implementation
     *
     * @return void
     * @group integration
     * @covers ::create
     */
    public function testCreate(): void
    {
        $dateTimeFactory = $this->createDateTimeFactory();
        $time = '1989-12-17T23:59:59+02:00';
        $dateTime = $dateTimeFactory->create($time);
        self::assertSame($time, $dateTime->format(DateTime::W3C));
    }

    /**
     * Tester of create for exception implementation
     *
     * @return void
     * @group negative
     * @covers ::create
     */
    public function testCreateException(): void
    {
        $code = 42;
        $message = 'Exception message';
        $throwable = $this->createThrowable($message, $code);
        $dateTimeFactory = $this->createDateTimeFactoryMock(['createTimeZone']);
        $dateTimeFactory->expects(self::once())->method('createTimeZone')->willThrowException($throwable);
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($message);
        $this->expectExceptionCode($code);
        $this->expectExceptionData();

        $dateTimeFactory->create();
    }

    /**
     * Data provider for testCreateFromFormat test
     *
     * @phpstan-return Generator<string, array{0: DateTime::*|string, 1: string, 2: ?string}>
     */
    public function createFromFormatProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="DateTime::ATOM">
        yield 'DateTime::ATOM' => [DateTime::ATOM, '1989-12-17T23:59:59+02:00', null];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="DateTime::ATOM_EXTENDED">
        yield 'DateTime::ATOM_EXTENDED' => [DateTime::ATOM_EXTENDED, '1989-12-17T23:59:59.599999+02:00', null];
        // </editor-fold>
    }

    /**
     * Tester of createFromFormat implementation
     *
     * @param string $format Format of the $string parameter should be in
     * @param string $time A string representing the DateTime
     * @param string|DateTimeZone|null $timeZone [OPTIONAL] Desired Timezone
     * @return void
     * @group integration
     * @dataProvider createFromFormatProvider
     * @covers ::createFromFormat
     */
    public function testCreateFromFormat(string $format, string $time, $timeZone = null): void
    {
        $dateTimeFactory = $this->createDateTimeFactory();
        $dateTime = $dateTimeFactory->createFromFormat($format, $time, $timeZone);
        self::assertSame($time, $dateTime->format($format));
    }

    /**
     * Data provider for testCreateFromFormat test
     *
     * @phpstan-return Generator<string, array{0: DateTime::*|string, 1: string, 2: ?string}>
     */
    public function createFromFormatErrorProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="empty string">
        yield 'empty string' => [DateTime::W3C, '', null];
        // </editor-fold>
    }

    /**
     * Teser of createFromFormat error implementation
     *
     * @param string $format Format of the $string parameter should be in
     * @param string $time A string representing the DateTime
     * @param string|DateTimeZone|null $timeZone [OPTIONAL] Desired Timezone
     * @return void
     * @group negative
     * @dataProvider createFromFormatErrorProvider
     * @covers ::createFromFormat
     */
    public function testCreateFromFormatError($format, $time, $timeZone = null): void
    {
        $message = 'Unable to create datetime object from ":dateTime", expected format is ":format"';
        $dateTimeFactory = $this->createDateTimeFactory();
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($message);
        $this->expectExceptionData([
            'dateTime' => $time,
            'format' => $format,
        ]);
        $dateTimeFactory->createFromFormat($format, $time, $timeZone);
    }

    /**
     * Data provider for testCreateFromXMLIsoDateTime test
     *
     * @phpstan-return Generator<DateTime::*|string, array{0: string, 1: string, 2: string}>
     */
    public function createFromIso8601Provider(): Generator
    {
        $timeZone = 'Europe/Prague';
        $pFormat = '+01:00';
        $oFormat = '+0100';

        // <editor-fold defaultstate="collapsed" desc="Y-m-d\TH:i:s">
        yield 'Y-m-d\TH:i:s' => ['1989-12-17T12:00:00', $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d\TH:i:s.u">
        yield 'Y-m-d\TH:i:s.u' => ['1989-12-17T12:00:00.000000', $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d\TH:i:sO - DateTime::ISO8601">
        yield DateTime::ISO8601 => ['1989-12-17T12:00:00' . $oFormat, $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d\TH:i:s.uO - DateTime::ISO8601_EXTENDED">
        yield DateTime::ISO8601_EXTENDED => ['1989-12-17T12:00:00.000000' . $oFormat, $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d\TH:i:sP - DateTime::ATOM">
        yield DateTime::ATOM => ['1989-12-17T12:00:00' . $pFormat, $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d\TH:i:s.uP - DateTime::ATOM_EXTENDED">
        yield DateTime::ATOM_EXTENDED => ['1989-12-17T12:00:00.000000' . $pFormat, $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d\TH:i:s O">
        yield 'Y-m-d\TH:i:s O' => ['1989-12-17T12:00:00 ' . $oFormat, $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d\TH:i:s.uO">
        yield 'Y-m-d\TH:i:s.u O' => ['1989-12-17T12:00:00.000000 ' . $oFormat, $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d\TH:i:s P">
        yield 'Y-m-d\TH:i:s P' => ['1989-12-17T12:00:00 ' . $pFormat, $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d\TH:i\Z">
        yield 'Y-m-d\TH:i\Z' => ['1989-12-17T12:00Z', 'UTC', '+00:00'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Ymd\THis\Z">
        yield 'Ymd\THis\Z' => ['19891217T120000Z', 'UTC', '+00:00'];
        // </editor-fold>
    }

    /**
     * Data provider for testCreateFromXMLIsoDateTime with space instead of T test
     *
     * @phpstan-return Generator<DateTime::*|string, array{0: string, 1: string, 2: string}>
     */
    public function createFromIso8601ProviderWithSpace(): Generator
    {
        $timeZone = 'Europe/Prague';
        $pFormat = '+01:00';
        $oFormat = '+0100';

        // <editor-fold defaultstate="collapsed" desc="Y-m-d H:i:s">
        yield 'Y-m-d H:i:s' => ['1989-12-17 12:00:00', $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d H:i:s.u">
        yield 'Y-m-d H:i:s.u' => ['1989-12-17 12:00:00.000000', $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d H:i:sO">
        yield 'Y-m-d H:i:sO' => ['1989-12-17 12:00:00' . $oFormat, $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d H:i:s.uO">
        yield 'Y-m-d H:i:s.uO' => ['1989-12-17 12:00:00.000000' . $oFormat, $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d H:i:sP">
        yield 'Y-m-d H:i:sP' => ['1989-12-17 12:00:00' . $pFormat, $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d H:i:s.uP">
        yield 'Y-m-d H:i:s.uP' => ['1989-12-17 12:00:00.000000' . $pFormat, $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d H:i:s O">
        yield 'Y-m-d H:i:s O' => ['1989-12-17 12:00:00 ' . $oFormat, $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d H:i:s.u O">
        yield 'Y-m-d H:i:s.u O' => ['1989-12-17 12:00:00.000000 ' . $oFormat, $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d H:i:s P">
        yield 'Y-m-d H:i:s P' => ['1989-12-17 12:00:00 ' . $pFormat, $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d H:i:s.u P">
        yield 'Y-m-d H:i:s.u P' => ['1989-12-17 12:00:00.000000 ' . $pFormat, $timeZone, $pFormat];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Y-m-d H:i\Z">
        yield 'Y-m-d H:i\Z' => ['1989-12-17 12:00Z', 'UTC', '+00:00'];
        // </editor-fold>
    }

    /**
     * Tester of createFromIso8601 implementation
     *
     * @param string $isoDateTime A string representing the DateTime in ISO 8601 format
     * @param string $timeZone Desired Timezone
     * @param string $pFormat
     * @return void
     * @group integration
     * @dataProvider createFromIso8601Provider
     * @dataProvider createFromIso8601ProviderWithSpace
     * @covers ::createFromIso8601
     * @covers \Interitty\Utils\RegexPattern::getIso8601Pattern
     */
    public function testCreateFromIso8601(string $isoDateTime, string $timeZone, string $pFormat): void
    {
        $expectedFormat = DateTime::ATOM_EXTENDED;
        $expectedResult = '1989-12-17T12:00:00.000000' . $pFormat;

        $dateTimeFactory = $this->createDateTimeFactory();
        $dateTime = $dateTimeFactory->createFromIso8601($isoDateTime, $timeZone);
        self::assertSame($expectedResult, $dateTime->format($expectedFormat));
    }

    /**
     * Data provider for createFromXMLIsoDateTimeError format test
     *
     * @phpstan-return Generator<string, array{0: string, 1: string, 2: array<string, string>}>
     */
    public function createFromIso8601FormatErrorProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="YYYY-DD-MM error">
        yield 'YYYY-DD-MM error' => [
            /** @phpstan-var string $isoDateTime */
            '1989-17-12T12:00:00',
            'Unable to create datetime object from ":dateTime", expected format is ":format"',
            ['dateTime' => '1989-17-12T12:00:00', 'format' => 'Y-m-d\TH:i:s.u'],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="YYYY-DD-MM error with timezone">
        yield 'YYYY-DD-MM error with timezone' => [
            /** @phpstan-var string $isoDateTime */
            '1989-17-12T12:00+00:00',
            'Unable to create datetime object from ":dateTime", expected format is ":format"',
            ['dateTime' => '1989-17-12T12:00+00:00', 'format' => 'Y-m-d\TH:i:s.uP'],
        ];
        // </editor-fold>
    }

    /**
     * Data provider for createFromXMLIsoDateTimeError pattern test
     *
     * @phpstan-return Generator<string, array{0: string, 1: string, 2: array<string, string>}>
     */
    public function createFromIso8601PatternErrorProvider(): Generator
    {
        /** @phpstan-var string $pattern */
        $pattern = RegexPattern::getIso8601Pattern();
        // <editor-fold defaultstate="collapsed" desc="Empty string">
        yield 'Empty string' => [
            '',
            'Unable to parse datetime from ":dateTime", string should match ":pattern"',
            ['dateTime' => '', 'pattern' => $pattern],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Not datetime string">
        yield 'Not datetime string' => [
            'test',
            'Unable to parse datetime from ":dateTime", string should match ":pattern"',
            ['dateTime' => 'test', 'pattern' => $pattern],
        ];
        // </editor-fold>
    }

    /**
     * Tester of createFromIso8601 error implementation
     *
     * @param string $isoDateTime A string representing the DateTime in ISO 8601 format
     * @param string $message
     * @phpstan-param array<string, mixed> $data
     * @return void
     * @group negative
     * @dataProvider createFromIso8601FormatErrorProvider
     * @dataProvider createFromIso8601PatternErrorProvider
     * @covers ::createFromIso8601
     */
    public function testCreateFromIso8601Error(string $isoDateTime, string $message, array $data): void
    {
        $dateTimeFactory = $this->createDateTimeFactory();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($message);
        $this->expectExceptionData($data);
        $dateTimeFactory->createFromIso8601($isoDateTime);
    }

    /**
     * Data provider for createTimeZone test
     *
     * @phpstan-return Generator<string, array{0: string|DateTimeZone|null, 1: string}>
     */
    public function createTimeZoneProvider(): Generator
    {
        $dateTimeFactory = $this->createDateTimeFactory();
        // <editor-fold defaultstate="collapsed" desc="null">
        yield 'null' => [null, date_default_timezone_get()];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Europe/Prague">
        yield 'Europe/Prague' => ['Europe/Prague', 'Europe/Prague'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="TimeZone object">
        yield 'TimeZone object' => [$dateTimeFactory->createTimeZone('UTC'), 'UTC'];
        // </editor-fold>
    }

    /**
     * Tester of createTimeZone implementation
     *
     * @param string|DateTimeZone|null $timeZone
     * @param string $expectedTimeZone
     * @return void
     * @group integration
     * @dataProvider createTimeZoneProvider
     * @covers ::createTimeZone
     */
    public function testCreateTimeZone($timeZone, string $expectedTimeZone): void
    {
        $dateTimeFactory = $this->createDateTimeFactory();
        self::assertSame($expectedTimeZone, $dateTimeFactory->createTimeZone($timeZone)->getName());
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * DateTimeFactory factory
     *
     * @return DateTimeFactory
     */
    protected function createDateTimeFactory(): DateTimeFactory
    {
        $dateTimeFactory = new DateTimeFactory();
        return $dateTimeFactory;
    }

    /**
     * DateTimeFactory mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return DateTimeFactory&MockObject
     */
    protected function createDateTimeFactoryMock(array $methods = []): MockObject
    {
        $mock = $this->createPartialMock(DateTimeFactory::class, $methods);
        return $mock;
    }

    /**
     * Throwable factory
     *
     * @param string $message [OPTIONAL]
     * @param int $code [OPTIONAL]
     * @return Throwable
     */
    protected function createThrowable(string $message = '', int $code = 0): Throwable
    {
        $throwable = $this->createMockAbstract(Throwable::class);
        $this->setNonPublicPropertyValue($throwable, 'message', $message);
        $this->setNonPublicPropertyValue($throwable, 'code', $code);
        return $throwable;
    }

    // </editor-fold>
}
