<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Attribute;
use Interitty\PhpUnit\BaseTestCase;
use Nette\PhpGenerator\Helpers;
use ReflectionException;

/**
 * @coversDefaultClass Interitty\Utils\ReflectionObject
 */
class ReflectionObjectTest extends BaseTestCase
{
    /** All available class / interface / namespace name constants */
    protected const string NAME_NAMESPACE = 'Vendor\\Namespace';
    protected const string NAME_ATTRIBUTE_OBJECT = self::NAME_NAMESPACE . '\\TestAttributeObject';
    protected const string NAME_ATTRIBUTED_OBJECT = self::NAME_NAMESPACE . '\\TestAttributedObject';
    protected const string NAME_NESTED_OBJECT = self::NAME_NAMESPACE . '\\TestNestedObject';
    protected const string NAME_OBJECT = self::NAME_NAMESPACE . '\\TestObject';

    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of methods attributes getter implementation
     *
     * @return void
     * @group integration
     * @covers ::getMethodsAttributes
     */
    public function testGetMethodsAttributes(): void
    {
        /**
         * @phpstan-var class-string $attributeClass
         * @phpstan-ignore varTag.nativeType
         */
        $attributeClass = self::NAME_ATTRIBUTE_OBJECT;
        $object = $this->generateAttributedObject(self::NAME_ATTRIBUTED_OBJECT, $attributeClass);

        /**
         * @var BaseAttribute[] $methods
         * @phpstan-ignore varTag.nativeType
         */
        $methods = ReflectionObject::getMethodsAttributes($object, $attributeClass);
        $methodNames = [];
        foreach ($methods as $attributte) {
            $methodNames[] = $attributte->getReflectionMethod()->getName();
        }
        self::assertSame(['firstMethod', 'secondMethod'], $methodNames);
    }

    /**
     * Tester of non-public property value getter
     *
     * @return void
     * @group integration
     * @covers ::getProperty
     * @covers ::getNonPublicPropertyValue
     */
    public function testGetNonPublicPropertyValue(): void
    {
        $value = 'foo';
        $object = $this->generateNestedObject(self::NAME_NESTED_OBJECT, self::NAME_OBJECT);
        $object->setProperty($value); // @phpstan-ignore-line
        self::assertSame($value, ReflectionObject::getNonPublicPropertyValue($object, 'property'));
    }

    /**
     * Tester of non-public property value setter
     *
     * @return void
     * @group integration
     * @covers ::getProperty
     * @covers ::setNonPublicPropertyValue
     */
    public function testSetNonPublicPropertyValue(): void
    {
        $value = 'foo';
        $object = $this->generateNestedObject(self::NAME_NESTED_OBJECT, self::NAME_OBJECT);
        ReflectionObject::setNonPublicPropertyValue($object, 'property', $value);
        self::assertSame($value, $object->getProperty()); // @phpstan-ignore-line
    }

    /**
     * Tester of property reflection exception getter for exception handle implementation
     *
     * @return void
     * @group negative
     * @covers ::getProperty
     */
    public function testGetPropertyException(): void
    {
        $property = 'foo';
        $object = $this->generateNestedObject(self::NAME_NESTED_OBJECT, self::NAME_OBJECT);
        $reflection = new ReflectionObject($object);

        $this->expectException(ReflectionException::class);
        $this->expectExceptionMessage('Property :class::$:property does not exist');
        $this->expectExceptionData(['class' => self::NAME_NESTED_OBJECT, 'property' => $property]);

        $reflection->getProperty($property, true);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Attribute object generator
     *
     * @param string $className
     * @return void
     */
    protected function generateAttributeObject(string $className): void
    {
        $classShortName = Helpers::extractShortName($className);
        $namespace = Strings::before($className, '\\' . $classShortName, -1);
        $code = '<?php
declare(strict_types=1);

' . ((string) $namespace === '' ? '' : 'namespace ' . $namespace . ';') . '

use ' . Attribute::class . ';
use ' . BaseAttribute::class . ';

#[Attribute(Attribute::TARGET_METHOD)]
class ' . $classShortName . ' extends BaseAttribute {

}';
        $this->processRegisterAutoload($className, $code);
    }

    /**
     * Attributed object generator
     *
     * @param string $className
     * @return object
     */
    protected function generateAttributedObject(string $className, string $attributeClassName): object
    {
        $this->generateAttributeObject($attributeClassName);
        $classShortName = Helpers::extractShortName($className);
        $namespace = Strings::before($className, '\\' . $classShortName, -1);
        $code = '<?php
declare(strict_types=1);

' . ((string) $namespace === '' ? '' : 'namespace ' . $namespace . ';') . '

use ' . $attributeClassName . ' as TestAttribute;

class ' . $classShortName . ' {
    #[TestAttribute]
    public function firstMethod(): void
    {
    }

    #[TestAttribute]
    public function secondMethod(): void
    {
    }
}';
        $this->processRegisterAutoload($className, $code);
        $object = new $className();
        return $object;
    }

    /**
     * Object with private property generator
     *
     * @param string $className
     * @return object
     */
    protected function generateNestedObject(string $className, string $parentClassName): object
    {
        $this->generateObject($parentClassName);
        $classShortName = Helpers::extractShortName($className);
        $parentClassShortName = Helpers::extractShortName($parentClassName);
        $namespace = Strings::before($className, '\\' . $classShortName, -1);
        $code = '<?php
declare(strict_types=1);

' . ((string) $namespace === '' ? '' : 'namespace ' . $namespace . ';') . '

class ' . $classShortName . ' extends ' . $parentClassShortName . ' {
}';
        $this->processRegisterAutoload($className, $code);
        $object = new $className();
        return $object;
    }

    /**
     * Object with private property generator
     *
     * @param string $className
     * @return void
     */
    protected function generateObject(string $className): void
    {
        $classShortName = Helpers::extractShortName($className);
        $namespace = Strings::before($className, '\\' . $classShortName, -1);
        $code = '<?php
declare(strict_types=1);

' . ((string) $namespace === '' ? '' : 'namespace ' . $namespace . ';') . '

class ' . $classShortName . ' {
    private ?string $property = null;

    public function getProperty(): ?string
    {
        return $this->property;
    }

    public function setProperty(string $value): void
    {
        $this->property = $value;
    }
}';
        $this->processRegisterAutoload($className, $code);
    }

    // </editor-fold>
}
