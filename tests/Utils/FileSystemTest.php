<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Interitty\PhpUnit\BaseTestCase;

use const DIRECTORY_SEPARATOR;

/**
 * @coversDefaultClass Interitty\Utils\FileSystem
 */
class FileSystemTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of copy implementation
     *
     * @return void
     * @covers ::copy
     */
    public function testCopy(): void
    {
        $rootFolder = $this->createTempDirectory();
        $originalPath = $this->createTempFile('', $rootFolder . DIRECTORY_SEPARATOR . 'original');
        $destinationPath = $rootFolder . DIRECTORY_SEPARATOR . 'destination' . DIRECTORY_SEPARATOR . 'file';
        self::assertFileDoesNotExist($destinationPath);
        FileSystem::copy($originalPath, $destinationPath);
        self::assertFileExists($destinationPath);
    }

    /**
     * Tester of create directory implementation
     *
     * @return void
     * @covers ::createDir
     */
    public function testCreateDir(): void
    {
        $rootFolder = $this->createTempDirectory();
        $path = $rootFolder . DIRECTORY_SEPARATOR . 'recursive' . DIRECTORY_SEPARATOR . 'folder';
        self::assertDirectoryDoesNotExist($path);
        FileSystem::createDir($path);
        self::assertDirectoryExists($path);
    }

    /**
     * Tester of delete implementation
     *
     * @return void
     * @covers ::delete
     */
    public function testDelete(): void
    {
        $rootFolder = $this->createTempDirectory();
        $filePath = $this->createTempFile('', $rootFolder . DIRECTORY_SEPARATOR . 'original');
        self::assertFileExists($filePath);
        FileSystem::delete($filePath);
        self::assertFileDoesNotExist($filePath);
    }

    /**
     * Tester of absolute checker implementation
     *
     * @return void
     * @covers ::isAbsolute
     */
    public function testIsAbsolute(): void
    {
        $rootFolder = $this->createTempDirectory();
        self::assertTrue(FileSystem::isAbsolute($rootFolder));
        self::assertFalse(FileSystem::isAbsolute('./'));
    }

    /**
     * Tester of join paths implementation
     *
     * @return void
     * @covers ::joinPaths
     */
    public function testJoinPaths(): void
    {
        $folder = 'folder';
        $file = 'file';
        $path = $folder . DIRECTORY_SEPARATOR . $file;
        self::assertSame($path, FileSystem::joinPaths($folder, $file));
    }

    /**
     * Tester of make writable implementation
     *
     * @return void
     * @covers ::makeWritable
     */
    public function testMakeWritable(): void
    {
        $rootFolder = $this->createTempDirectory();
        $path = $rootFolder . DIRECTORY_SEPARATOR . 'folder';
        $file = $path . DIRECTORY_SEPARATOR . 'file';
        FileSystem::write($file, '');
        FileSystem::makeWritable($file, 0700, 0600);
        self::assertDirectoryIsWritable($path);
        self::assertFileIsWritable($file);
    }

    /**
     * Tester of normalize path implementation
     *
     * @return void
     * @covers ::normalizePath
     */
    public function testNormalizePath(): void
    {
        $rootPath = '/';
        $path = $rootPath . DIRECTORY_SEPARATOR . 'test' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR;
        self::assertSame($rootPath, FileSystem::normalizePath($path));
    }

    /**
     * Tester of read implementation
     *
     * @return void
     * @covers ::read
     */
    public function testRead(): void
    {
        $content = 'content';
        $rootFolder = $this->createTempDirectory();
        $originalPath = $this->createTempFile($content, $rootFolder . DIRECTORY_SEPARATOR . 'original');
        self::assertSame($content, FileSystem::read($originalPath));
    }

    /**
     * Tester of rename implementation
     *
     * @return void
     * @covers ::rename
     */
    public function testRename(): void
    {
        $rootFolder = $this->createTempDirectory();
        $originalPath = $this->createTempFile('', $rootFolder . DIRECTORY_SEPARATOR . 'original');
        $destinationPath = $rootFolder . DIRECTORY_SEPARATOR . 'destination' . DIRECTORY_SEPARATOR . 'file';
        self::assertFileExists($originalPath);
        self::assertFileDoesNotExist($destinationPath);
        FileSystem::rename($originalPath, $destinationPath);
        self::assertFileDoesNotExist($originalPath);
        self::assertFileExists($destinationPath);
    }

    /**
     * Tester of tempnam implementation
     *
     * @return void
     * @covers ::tempnam
     */
    public function testTempnam(): void
    {
        self::assertFalse(FileSystem::tempnam('/non/exists/folder', 'prefix'));
        $path = $this->createTempDirectory();
        $prefix = 'folder' . DIRECTORY_SEPARATOR . 'prefix';
        $suffix = '.ssuffix';
        $tempFile = (string) FileSystem::tempnam($path, $prefix . $suffix);
        self::assertFileExists($tempFile);
        self::assertStringStartsWith($path . DIRECTORY_SEPARATOR . $prefix . '.', $tempFile);
        self::assertStringEndsWith($suffix, $tempFile);
    }

    /**
     * Tester of write implementation
     *
     * @return void
     * @covers ::write
     */
    public function testWrite(): void
    {
        $content = 'content';
        $rootFolder = $this->createTempDirectory();
        $originalPath = $this->createTempFile('', $rootFolder . DIRECTORY_SEPARATOR . 'original');
        FileSystem::write($originalPath, $content);
        self::assertSame($content, FileSystem::read($originalPath));
    }

    // </editor-fold>
}
