<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Generator;
use Interitty\PhpUnit\BaseTestCase;
use Nette\Utils\AssertionException;

use function chmod;

use const FILTER_VALIDATE_BOOL;
use const FILTER_VALIDATE_DOMAIN;
use const FILTER_VALIDATE_EMAIL;
use const FILTER_VALIDATE_INT;
use const FILTER_VALIDATE_IP;
use const FILTER_VALIDATE_MAC;
use const FILTER_VALIDATE_URL;

/**
 * @coversDefaultClass Interitty\Utils\Validators
 */
class ValidatorsTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of check
     *
     * @return void
     * @covers ::check
     * @group integration
     */
    public function testCheck(): void
    {
        $test = 'string';
        static::assertTrue(Validators::check($test, 'string'));
    }

    /**
     * Tester of checkField
     *
     * @return void
     * @covers ::checkField
     * @group integration
     */
    public function testCheckField(): void
    {
        $test = [
            'field' => 'string',
        ];
        static::assertTrue(Validators::checkField($test, 'field', 'string'));
    }

    /**
     * Tester of `executable` assert
     *
     * @return void
     * @covers ::assert
     * @group integration
     */
    public function testAssertExecutable(): void
    {
        $file = $this->createTempFile('', 'file');
        chmod($file, 0755);
        static::assertTrue(Validators::check($file, 'executable', 'file'));
    }

    /**
     * Tester of `false` and `uninitialized` assert
     *
     * @return void
     * @covers ::assert
     * @covers ::isFalse
     * @group integration
     */
    public function testAssertFalse(): void
    {
        $object = (object) [];
        static::assertTrue(Validators::check(false, 'false'));
        static::assertTrue(Validators::check(isset($object->property), 'uninitialized'));
    }

    /**
     * Tester of `executable` assert for exception handle
     *
     * @return void
     * @covers ::assert
     * @group integration
     * @group negative
     */
    public function testAssertNotExecutable(): void
    {
        $label = 'file';
        $expected = 'executable';
        $file = $this->createTempFile('', 'file');
        chmod($file, 0000);
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('The :label expects to be :expected, :type given');
        $this->expectExceptionData(['expected' => $expected, 'label' => $label, 'type' => 'string \'' . $file . '\'']);
        static::assertTrue(Validators::check($file, $expected, $label));
    }

    /**
     * Tester of `false` assert for exception handle
     *
     * @return void
     * @covers ::assert
     * @covers ::isFalse
     * @group integration
     * @group negative
     */
    public function testAssertNotFalse(): void
    {
        $label = 'label';
        $expected = 'false';
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('The :label expects to be :expected, :type given');
        $this->expectExceptionData(['expected' => $expected, 'label' => $label, 'type' => 'bool true']);
        static::assertTrue(Validators::check(true, $expected, $label));
    }

    /**
     * Tester of `initialized` assert for exception handle
     *
     * @return void
     * @covers ::assert
     * @covers ::isTrue
     * @group integration
     * @group negative
     */
    public function testAssertNotInitialized(): void
    {
        $label = 'label';
        $expected = 'initialized';
        $object = (object) [];
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('The :label expects to be :expected');
        $this->expectExceptionData(['expected' => $expected, 'label' => $label]);
        static::assertTrue(Validators::check(isset($object->property), $expected, $label));
    }

    /**
     * Tester of `readable` assert for exception handle
     *
     * @return void
     * @covers ::assert
     * @group integration
     * @group negative
     */
    public function testAssertNotReadable(): void
    {
        $label = 'file';
        $expected = 'readable';
        $file = $this->createTempFile('', 'file');
        chmod($file, 0000);
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('The :label expects to be :expected, :type given');
        $this->expectExceptionData(['expected' => $expected, 'label' => $label, 'type' => 'string \'' . $file . '\'']);
        static::assertTrue(Validators::check($file, $expected, $label));
    }

    /**
     * Tester of `true` assert for exception handle
     *
     * @return void
     * @covers ::assert
     * @covers ::isTrue
     * @group integration
     * @group negative
     */
    public function testAssertNotTrue(): void
    {
        $label = 'label';
        $expected = 'true';
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('The :label expects to be :expected, :type given');
        $this->expectExceptionData(['expected' => $expected, 'label' => $label, 'type' => 'bool false']);
        static::assertTrue(Validators::check(false, $expected, $label));
    }

    /**
     * Tester of `uninitialized` assert for exception handle
     *
     * @return void
     * @covers ::assert
     * @covers ::isFalse
     * @group integration
     * @group negative
     */
    public function testAssertNotUninitialized(): void
    {
        $label = 'label';
        $expected = 'uninitialized';
        $object = (object) ['property' => 42];
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('The :label expects to be :expected');
        $this->expectExceptionData(['expected' => $expected, 'label' => $label]);
        static::assertTrue(Validators::check(isset($object->property), $expected, $label));
    }

    /**
     * Tester of `writable` assert for exception handle
     *
     * @return void
     * @covers ::assert
     * @group integration
     * @group negative
     */
    public function testAssertNotWritable(): void
    {
        $label = 'file';
        $expected = 'writable';
        $file = $this->createTempFile('', 'file');
        chmod($file, 0000);
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('The :label expects to be :expected, :type given');
        $this->expectExceptionData(['expected' => $expected, 'label' => $label, 'type' => 'string \'' . $file . '\'']);
        static::assertTrue(Validators::check($file, $expected, $label));
    }

    /**
     * Tester of `readable` assert
     *
     * @return void
     * @covers ::assert
     * @group integration
     */
    public function testAssertReadable(): void
    {
        $file = $this->createTempFile('', 'file');
        static::assertTrue(Validators::check($file, 'readable', 'file'));
    }

    /**
     * Tester of `true` and `initialized` assert
     *
     * @return void
     * @covers ::assert
     * @covers ::isTrue
     * @group integration
     */
    public function testAssertTrue(): void
    {
        $object = (object) ['property' => 42];
        static::assertTrue(Validators::check(true, 'true'));
        static::assertTrue(Validators::check(isset($object->property), 'initialized'));
    }

    /**
     * Tester of `writable` assert
     *
     * @return void
     * @covers ::assert
     * @group integration
     */
    public function testAssertWritable(): void
    {
        $file = $this->createTempFile('', 'file');
        static::assertTrue(Validators::check($file, 'writable', 'file'));
    }

    /**
     * DataProvider with constants for testFilterVariable test
     *
     * @return Generator<string, array{0: mixed, 1: mixed, 2: int, 3?: string[]}>
     */
    public function filterVariableProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="boolean">
        yield 'boolean_constant' => [true, true, FILTER_VALIDATE_BOOL];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="integer">
        yield 'integer_constant' => [42, 42, FILTER_VALIDATE_INT];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="float">
        yield 'float_constant' => [4.2, 4.2, FILTER_VALIDATE_FLOAT];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="email">
        yield 'email_constant' => ['info@interitty.org', 'info@interitty.org', FILTER_VALIDATE_EMAIL];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="url">
        yield 'url_constant' => ['https://interitty.org', 'https://interitty.org', FILTER_VALIDATE_URL];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="domain">
        yield 'domain_constant' => ['interitty.org', 'interitty.org', FILTER_VALIDATE_DOMAIN];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ip">
        yield 'ip_constant' => ['127.0.0.1', '127.0.0.1', FILTER_VALIDATE_IP];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="mac">
        yield 'mac_constant' => ['22:34:28:61:af:33', '22:34:28:61:af:33', FILTER_VALIDATE_MAC];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="regexp">
        yield 'regexp_constant' => ['42', '42', FILTER_VALIDATE_REGEXP, ['regexp' => '~42~']];
        // </editor-fold>
    }

    /**
     * DataProvider with flags for testFilterVariable test
     *
     * @return Generator<string, array{0: null, 1: string, 2: int, 3: null, 4: int|int[]}>
     */
    public function filterVariableFlagsProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="integer flags">
        yield 'integer flags' => [
            null, '192.168.0.1', FILTER_VALIDATE_IP, null, FILTER_FLAG_NO_PRIV_RANGE | FILTER_NULL_ON_FAILURE,
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="array flags">
        yield 'array flags' => [
            null, '192.168.0.1', FILTER_VALIDATE_IP, null, [FILTER_FLAG_NO_PRIV_RANGE, FILTER_NULL_ON_FAILURE],
        ];
        // </editor-fold>
    }

    /**
     * DataProvider with wrong data for testFilterVariable test
     *
     * @return Generator<string, array{0: mixed, 1: mixed, 2: int, 3?: string[]}>
     */
    public function filterVariableWrongProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="boolean">
        yield 'boolean_wrong_constant' => [false, 'wrong', FILTER_VALIDATE_BOOL];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="integer">
        yield 'integer_wrong_constant' => [false, 'wrong', FILTER_VALIDATE_INT];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="float">
        yield 'float_wrong_constant' => [false, 'wrong', FILTER_VALIDATE_FLOAT];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="email">
        yield 'email_wrong_constant' => [false, 'wrong', FILTER_VALIDATE_EMAIL];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="url">
        yield 'url_wrong_constant' => [false, 'wrong', FILTER_VALIDATE_URL];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="domain">
        yield 'domain_wrong_constant' => [false, '.', FILTER_VALIDATE_DOMAIN];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ip">
        yield 'ip_wrong_constant' => [false, 'wrong', FILTER_VALIDATE_IP];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="mac">
        yield 'mac_wrong_constant' => [false, 'wrong', FILTER_VALIDATE_MAC];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="regexp">
        yield 'regexp_wrong_constant' => [false, 'wrong', FILTER_VALIDATE_REGEXP, ['regexp' => '~42~']];
        // </editor-fold>
    }

    /**
     * Tester of filterVariable implementation
     *
     * @param mixed $expected
     * @param mixed $value
     * @param int $filter
     * @param mixed $options
     * @param int|int[] $flags
     * @return void
     * @covers ::filterVariable
     * @dataProvider filterVariableProvider
     * @dataProvider filterVariableFlagsProvider
     * @dataProvider filterVariableWrongProvider
     * @group integration
     */
    public function testFilterVariable(
        mixed $expected,
        mixed $value,
        int $filter,
        mixed $options = null,
        int|array $flags = null
    ): void {
        static::assertSame($expected, Validators::filterVariable($value, $filter, $options, $flags));
    }

    /**
     * Tester of filterVariable for default value implementation
     *
     * @return void
     * @covers ::filterVariable
     * @group integration
     */
    public function testFilterVariableDefault(): void
    {
        static::assertNull(Validators::filterVariable(null));
        static::assertSame('default', Validators::filterVariable(null, options: 'default'));
        static::assertSame('default', Validators::filterVariable(null, options: ['default' => 'default']));
    }

    // </editor-fold>
}
