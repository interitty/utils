<?php

declare(strict_types=1);

namespace Interitty\Utils;

use BadMethodCallException;
use Interitty\PhpUnit\BaseTestCase;
use Nette\InvalidArgumentException;
use PHPUnit\Framework\MockObject\MockObject;

use function count;

/**
 * @coversDefaultClass Interitty\Utils\ArrayWrapper
 */
class ArrayWrapperTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of undefined method call exception
     *
     * @return void
     * @group negative
     * @covers ::__call
     */
    public function testCallUndefinedMethod(): void
    {
        $arrayWrapper = ArrayWrapper::create();
        $this->expectException(BadMethodCallException::class);
        $this->expectExceptionMessage('Call to undefined method :class:::method()');
        $this->expectExceptionData([
            'class' => ArrayWrapper::class,
            'method' => 'undefinedMethod',
        ]);
        $arrayWrapper->undefinedMethod(); // @phpstan-ignore method.notFound
    }

    /**
     * Tester of count implementation
     *
     * @return void
     * @covers ::count
     */
    public function testCount(): void
    {
        $array = ['value'];
        $arrayWrapper = ArrayWrapper::create($array);
        self::assertCount(count($array), $arrayWrapper);
    }

    /**
     * Tester of Static factory implementation
     *
     * @return void
     * @group integration
     * @covers ::create
     */
    public function testCreate(): void
    {
        $array = [];
        $arrayWrapperOne = ArrayWrapper::create($array);
        $arrayWrapperTwo = ArrayWrapper::create($array);
        self::assertNotSame($arrayWrapperOne, $arrayWrapperTwo);
    }

    /**
     * Tester of magic getters setters implementation
     *
     * @return void
     * @group integration
     * @covers ::__call
     */
    public function testGettersSettersAccess(): void
    {
        $value = 'value';
        /**
         * @phpstan-var ArrayWrapper<array{'key': string}> $arrayWrapper
         * @phpstan-ignore varTag.type
         */
        $arrayWrapper = ArrayWrapper::create();
        self::assertFalse($arrayWrapper->isKey());
        self::assertFalse($arrayWrapper->hasKey());
        $arrayWrapper->setKey($value);
        self::assertTrue($arrayWrapper->isKey());
        self::assertTrue($arrayWrapper->hasKey());
        self::assertEquals($value, $arrayWrapper->getKey());
        $arrayWrapper->unsetKey();
        self::assertFalse($arrayWrapper->isKey());
        self::assertFalse($arrayWrapper->hasKey());
    }

    /**
     * Tester of offset methods access implementation
     *
     * @return void
     * @group integration
     * @covers ::offsetExists
     * @covers ::offsetGet
     * @covers ::offsetSet
     * @covers ::offsetUnset
     */
    public function testOffsetAccess(): void
    {
        $offset = 'key';
        $value = 'value';
        /**
         * @phpstan-var ArrayWrapper<array{key: string}> $arrayWrapper
         * @phpstan-ignore varTag.type
         */
        $arrayWrapper = ArrayWrapper::create();
        self::assertFalse(isset($arrayWrapper[$offset]));
        $arrayWrapper[$offset] = $value;
        self::assertTrue(isset($arrayWrapper[$offset]));
        self::assertSame($value, $arrayWrapper[$offset]);
        unset($arrayWrapper[$offset]);
        self::assertFalse(isset($arrayWrapper[$offset]));
    }

    /**
     * Tester of offset methods deep access implementation
     *
     * @return void
     * @group integration
     * @covers ::offsetExists
     * @covers ::offsetGet
     * @covers ::offsetSet
     * @covers ::offsetUnset
     */
    public function testOffsetDeepAccess(): void
    {
        $offset = ['deep', 'key'];
        $value = 'value';
        /**
         * @phpstan-var ArrayWrapper<array{deep: array{key: string}}> $arrayWrapper
         * @phpstan-ignore varTag.type
         */
        $arrayWrapper = ArrayWrapper::create();
        self::assertFalse(isset($arrayWrapper[$offset]));
        $arrayWrapper[$offset] = $value;
        self::assertTrue(isset($arrayWrapper[$offset]));
        self::assertSame($value, $arrayWrapper[$offset]);
        unset($arrayWrapper[$offset]);
        self::assertFalse(isset($arrayWrapper[$offset]));
    }

    /**
     * Tester of offset methods default value access implementation
     *
     * @return void
     * @group integration
     * @covers ::offsetGet
     */
    public function testOffsetDefaultAccess(): void
    {
        $offset = 'key';
        $default = 'value';
        /**
         * @phpstan-var ArrayWrapper<array{key: string}> $arrayWrapper
         * @phpstan-ignore varTag.type
         */
        $arrayWrapper = ArrayWrapper::create();
        self::assertFalse($arrayWrapper->offsetExists($offset));
        self::assertSame($default, $arrayWrapper->offsetGet($offset, $default));
    }

    /**
     * Tester of offset methods not exist value access implementation
     *
     * @return void
     * @group negative
     * @covers ::offsetGet
     */
    public function testOffsetNotExistAccess(): void
    {
        $offset = 'key';
        $arrayWrapper = ArrayWrapper::create();
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Missing item ":item"');
        $this->expectExceptionData(['item' => '[' . $offset . ']']);
        $arrayWrapper->offsetGet($offset);
    }

    /**
     * Tester of properties access implementation
     *
     * @return void
     * @group integration
     * @covers ::__get
     * @covers ::__isset
     * @covers ::__set
     * @covers ::__unset
     */
    public function testPropertiesAccess(): void
    {
        $offset = 'key';
        $value = 'value';
        $arrayWrapper = ArrayWrapper::create();
        self::assertFalse(isset($arrayWrapper->{$offset})); // @phpstan-ignore property.notFound
        $arrayWrapper->{$offset} = $value; //// @phpstan-ignore property.notFound
        self::assertTrue(isset($arrayWrapper->{$offset}));
        self::assertSame($value, $arrayWrapper->{$offset});
        unset($arrayWrapper->{$offset});
        self::assertFalse(isset($arrayWrapper->{$offset})); // @phpstan-ignore property.notFound
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit Tests">
    /**
     * Tester of Constructor implementation
     *
     * @return void
     * @covers ::__construct
     */
    public function testConstructor(): void
    {
        $array = [];
        $arrayWrapper = $this->createArrayWrapperMock(['setArray']);
        $arrayWrapper->expects(self::once())->method('setArray')->with(self::equalTo($array));
        $arrayWrapper->__construct($array);
    }

    /**
     * Tester of array getter/setter implementation
     *
     * @return void
     * @covers ::getArray
     * @covers ::setArray
     */
    public function testGetSetArray(): void
    {
        $array = [];
        $this->processTestGetSet(ArrayWrapper::class, 'array', $array);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * ArrayWrapper mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return ArrayWrapper<mixed[]>&MockObject
     */
    protected function createArrayWrapperMock(array $methods = []): MockObject
    {
        $mock = $this->createPartialMock(ArrayWrapper::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
