<?php

declare(strict_types=1);

namespace Interitty\Utils;

use ArrayAccess;
use Generator;
use Interitty\PhpUnit\BaseTestCase;
use Nette\Utils\ArrayHash;
use RuntimeException;

use function array_keys;
use function array_reverse;
use function array_values;
use function range;

/**
 * @coversDefaultClass Interitty\Utils\ArraySorter
 */
class ArraySorterTest extends BaseTestCase
{
    /**
     * Data provider for sort descending test
     *
     * @return Generator<string, array{
     *      0: array<array-key, mixed[]>,
     *      1: array<array-key, mixed[]>,
     *      2: int[]|int|string[]|string,
     *      3: ArraySorter::FALLBACK_*
     *   }>
     */
    public function rsortDataProvider(): Generator
    {
        /** @var array<int, array{'id': int, 'meta': array{'title': string}}> $array */
        $array = [
            0 => ['id' => 0, 'meta' => ['title' => 'a']],
            1 => ['id' => 1, 'meta' => ['title' => 'b']],
            2 => ['id' => 2, 'meta' => ['title' => 'c']],
            3 => ['id' => 3, 'meta' => ['title' => 'd']],
        ];
        /** @var array<int, array{'id': int}> $empty */
        $empty = [4 => ['id' => 4]];

        $reverseArray = array_reverse($array, true);

        // <editor-fold defaultstate="collapsed" desc="No changes">
        yield 'No changes' => [$array, $reverseArray, ['meta', 'title'], ArraySorter::FALLBACK_BOTTOM];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Remove">
        yield 'Remove' => [$array + $empty, $reverseArray, ['meta', 'title'], ArraySorter::FALLBACK_REMOVE];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Top">
        yield 'Top' => [$array + $empty, $empty + $reverseArray, ['meta', 'title'], ArraySorter::FALLBACK_TOP];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Bottom">
        yield 'Bottom' => [$empty + $array, $reverseArray + $empty, ['meta', 'title'], ArraySorter::FALLBACK_BOTTOM];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Keep">
        yield 'Keep' => [$empty + $array, $empty + $reverseArray, ['meta', 'title'], ArraySorter::FALLBACK_KEEP];
        // </editor-fold>
    }

    /**
     * Tester of sort descending implementation
     *
     * @param array<array-key, mixed[]> $array
     * @param array<array-key, mixed[]> $expected
     * @param int[]|int|string[]|string $sortKey
     * @param ArraySorter::FALLBACK_* $fallback
     * @return void
     * @covers ::rsort
     * @covers ::uksort
     * @covers Interitty\Utils\Arrays::rsort
     * @covers ::processSort
     * @group integration
     * @dataProvider rsortDataProvider
     */
    public function testRsort($array, array $expected, $sortKey, $fallback): void
    {
        $result = Arrays::rsort($array, $sortKey, $fallback);
        self::assertSame(array_keys($expected), array_keys($result));
        self::assertSame(array_values($expected), array_values($result));
    }

    /**
     * Data provider for sort ascending test
     *
     * @return Generator<string, array{
     *      0: array<array-key, mixed[]>,
     *      1: array<array-key, mixed[]>,
     *      2: int[]|int|string[]|string,
     *      3: ArraySorter::FALLBACK_*
     *   }>
     */
    public function sortDataProvider(): Generator
    {
        /** @var array<int, array{'id': int, 'meta': array{'title': string}}> $array */
        $array = [
            0 => ['id' => 0, 'meta' => ['title' => 'a']],
            1 => ['id' => 1, 'meta' => ['title' => 'b']],
            2 => ['id' => 2, 'meta' => ['title' => 'c']],
            3 => ['id' => 3, 'meta' => ['title' => 'd']],
        ];
        /** @var array<int, array{'id': int}> $empty */
        $empty = [4 => ['id' => 4]];

        // <editor-fold defaultstate="collapsed" desc="No changes">
        yield 'No changes' => [$array, $array, ['meta', 'title'], ArraySorter::FALLBACK_BOTTOM];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Remove">
        yield 'Remove' => [$array + $empty, $array, ['meta', 'title'], ArraySorter::FALLBACK_REMOVE];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Top">
        yield 'Top' => [$array + $empty, $empty + $array, ['meta', 'title'], ArraySorter::FALLBACK_TOP];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Bottom">
        yield 'Bottom' => [$empty + $array, $array + $empty, ['meta', 'title'], ArraySorter::FALLBACK_BOTTOM];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Keep">
        yield 'Keep' => [$empty + $array, $empty + $array, ['meta', 'title'], ArraySorter::FALLBACK_KEEP];
        // </editor-fold>
    }

    /**
     * Tester of sort ascending implementation
     *
     * @param array<array-key, mixed[]> $array
     * @param array<array-key, mixed[]> $expected
     * @param int[]|int|string[]|string $sortKey
     * @param ArraySorter::FALLBACK_* $fallback
     * @return void
     * @covers ::sort
     * @covers Interitty\Utils\Arrays::sort
     * @covers ::processSort
     * @group integration
     * @dataProvider sortDataProvider
     */
    public function testSort($array, array $expected, $sortKey, $fallback): void
    {
        $result = Arrays::sort($array, $sortKey, $fallback);
        self::assertSame(array_keys($expected), array_keys($result));
        self::assertSame(array_values($expected), array_values($result));
    }

    /**
     * Data provider for tsort test
     *
     * @return Generator<string, array{
     *      0: mixed[],
     *      1: mixed[],
     *      2?: mixed[],
     *      3?: mixed[],
     *  }>
     */
    public function tsortDataProvider(): Generator
    {
        $array = range(0, 4);

        // <editor-fold defaultstate="collapsed" desc="Empty">
        yield 'Empty' => [[], []];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="No changes">
        yield 'No changes' => [$array, $array];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Before simple">
        yield 'Before simple' => [$array, $array, [1 => [2]]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="After multiple">
        yield 'After multiple' => [$array, $array, [], [3 => [1, 2]]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Non-exists">
        yield 'Non-exists' => [$array, $array, [6 => [7]], [8 => [9]]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="After reordered">
        yield 'After reordered' => [$array, [0, 2, 3, 4, 1], [], [1 => [2, 3]]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Before reordered">
        yield 'Before reordered' => [$array, [0, 3, 4, 1, 2], [3 => [2, 1]]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Combined">
        yield 'Combined' => [$array, [4, 3, 2, 1, 0], [3 => [2, 1], 2 => [1]], [3 => [4], 0 => [1, 2]]];
        // </editor-fold>
    }

    /**
     * Data provider for tsort test
     *
     * @return Generator<string, array{
     *      0: (iterable<mixed>&ArrayAccess<array-key, mixed>),
     *      1: mixed[],
     *      2?: mixed[],
     *      3?: mixed[],
     *  }>
     */
    public function tsortIterableDataProvider(): Generator
    {
        $array = range(0, 4);
        // <editor-fold defaultstate="collapsed" desc="Empty">
        yield 'Empty' => [ArrayHash::from([]), []];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="No changes">
        yield 'No changes' => [ArrayHash::from($array), $array];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Before simple">
        yield 'Before simple' => [ArrayHash::from($array), $array, [1 => [2]]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="After multiple">
        yield 'After multiple' => [ArrayHash::from($array), $array, [], [3 => [1, 2]]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Non-exists">
        yield 'Non-exists' => [ArrayHash::from($array), $array, [6 => [7]], [8 => [9]]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="After reordered">
        yield 'After reordered' => [ArrayHash::from($array), [0, 2, 3, 4, 1], [], [1 => [2, 3]]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Before reordered">
        yield 'Before reordered' => [ArrayHash::from($array), [0, 3, 4, 1, 2], [3 => [2, 1]]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Combined">
        yield 'Combined' => [
            ArrayHash::from($array),
            [4, 3, 2, 1, 0],
            [3 => [2, 1], 2 => [1]],
            [3 => [4], 0 => [1, 2]],
        ];
        // </editor-fold>
    }

    /**
     * Tester of tsort implementation
     *
     * @param array<array-key, mixed>|(ArrayAccess<array-key, mixed>&iterable<array-key, mixed>) $array
     * @param array<array-key, mixed> $expected
     * @param array<array-key, array<array-key>> $befores [OPTIONAL]
     * @param array<array-key, array<array-key>> $afters [OPTIONAL]
     * @return void
     * @covers ::tsort
     * @covers Interitty\Utils\Arrays::tsort
     * @covers ::processTsort
     * @group integration
     * @dataProvider tsortDataProvider
     * @dataProvider tsortIterableDataProvider
     */
    public function testTsort($array, array $expected, array $befores = [], array $afters = []): void
    {
        $result = Arrays::tsort($array, $befores, $afters);
        self::assertSame($expected, array_keys($result));
        self::assertSame($expected, array_values($result));
    }

    /**
     * Tester of tsort cycle prevention implementation
     *
     * @return void
     * @covers ::tsort
     * @covers Interitty\Utils\Arrays::tsort
     * @covers ::processTsort
     * @group negative
     */
    public function testTsortCycle(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Cycle found in tsort');
        $this->expectExceptionData(['restKeys' => [1 => [0], 0 => [1]]]);
        Arrays::tsort([0, 1, 2], [0 => [1], 1 => [0], 2 => [0]]);
    }
}
