<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Generator;
use Interitty\PhpUnit\BaseTestCase;
use Nette\InvalidArgumentException;
use Nette\Utils\Json;
use Nette\Utils\JsonException;

use const JSON_PRETTY_PRINT;

/**
 * @coversDefaultClass Interitty\Utils\Arrays
 */
class ArraysTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Data provider for array column test
     *
     * @return Generator<string, array{
     *      0: mixed[]|object,
     *      1: int[]|int|string[]|string,
     *      2: int[]|int|string[]|string|null,
     *      3: mixed[]
     *  }>
     */
    protected function arrayColumnProvider(): Generator
    {
        $array = [
            1 => ['key' => 'key1', 'value' => 'value1'],
            2 => ['key' => 'key2', 'value' => 'value2'],
        ];
        // <editor-fold defaultstate="collapsed" desc="Simple array column">
        yield 'Simple array column' => [
            $array,
            'value',
            null,
            [1 => 'value1', 2 => 'value2'],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Simple array column with index key">
        yield 'Simple array column with index key' => [
            $array,
            'value',
            'key',
            ['key1' => 'value1', 'key2' => 'value2'],
        ];
        // </editor-fold>
    }

    /**
     * Data provider for deep array column test
     *
     * @return Generator<string, array{
     *      0: mixed[]|object,
     *      1: int[]|int|string[]|string,
     *      2: int[]|int|string[]|string|null,
     *      3: mixed[]
     *  }>
     */
    protected function arrayColumnDeepProvider(): Generator
    {
        $array = [
            1 => ['data' => ['key' => 'key1', 'value' => 'value1']],
            2 => ['data' => ['key' => 'key2', 'value' => 'value2']],
        ];
        // <editor-fold defaultstate="collapsed" desc="Deep array column">
        yield 'Deep array column' => [
            $array,
            ['data', 'value'],
            null,
            [1 => 'value1', 2 => 'value2'],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Deep array column with index key">
        yield 'Deep array column with index key' => [
            $array,
            ['data', 'value'],
            ['data', 'key'],
            ['key1' => 'value1', 'key2' => 'value2'],
        ];
        // </editor-fold>
    }

    /**
     * Data provider with generator for array column test
     *
     * @return Generator<string, array{
     *      0: mixed[]|object,
     *      1: int[]|int|string[]|string,
     *      2: int[]|int|string[]|string|null,
     *      3: mixed[]
     *  }>
     */
    protected function arrayColumnGeneratorProvider(): Generator
    {
        $array = [
            1 => ['key' => 'key1', 'value' => 'value1'],
            2 => ['key' => 'key2', 'value' => 'value2'],
        ];
        // <editor-fold defaultstate="collapsed" desc="Simple array column">
        yield 'Simple array column' => [
            Arrays::arrayToGenerator($array),
            'value',
            null,
            [1 => 'value1', 2 => 'value2'],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Simple array column with index key">
        yield 'Simple array column with index key' => [
            Arrays::arrayToGenerator($array),
            'value',
            'key',
            ['key1' => 'value1', 'key2' => 'value2'],
        ];
        // </editor-fold>
    }

    /**
     * Data provider with missing data for array column test
     *
     * @return Generator<string, array{
     *      0: mixed[]|object,
     *      1: int[]|int|string[]|string,
     *      2: int[]|int|string[]|string|null,
     *      3: mixed[]
     *  }>
     */
    protected function arrayColumnMissingDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="Simple array column with missing value">
        yield 'Simple array column with missing value' => [
            [
                1 => ['key' => 'key1', 'value' => 'value1'],
                2 => ['key' => 'key2', 'value' => 'value2'],
                3 => ['key' => 'key3'],
            ],
            'value',
            null,
            [1 => 'value1', 2 => 'value2'],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Simple array column with missing key">
        yield 'Simple array column with missing key' => [
            [
                1 => ['key' => 'key1', 'value' => 'value1'],
                2 => ['key' => 'key2', 'value' => 'value2'],
                3 => ['value' => 'value3'],
            ],
            'value',
            'key',
            ['key1' => 'value1', 'key2' => 'value2', 0 => 'value3'],
        ];
        // </editor-fold>
    }

    /**
     * Tester of Array column helper implementation
     *
     * @param mixed[]|iterable|object $array
     * @param int[]|int|string[]|string $columnKey
     * @param int[]|int|string[]|string|null $indexKey
     * @param mixed $expected
     * @return void
     * @covers ::arrayColumn
     * @phpstan-param iterable<iterable<mixed>|object> $array
     * @group integration
     * @dataProvider arrayColumnProvider
     * @dataProvider arrayColumnDeepProvider
     * @dataProvider arrayColumnGeneratorProvider
     * @dataProvider arrayColumnMissingDataProvider
     */
    public function testArrayColumn($array, $columnKey, $indexKey, $expected): void
    {
        self::assertSame($expected, Arrays::arrayColumn($array, $columnKey, $indexKey));
    }

    /**
     * Data provider with array data for testOffsetGet test
     *
     * @return Generator<string, array{0: mixed[]|object, 1: int[]|int|string[]|string, 2: mixed}>
     */
    protected function offsetGetArrayProvider(): Generator
    {
        $array = ['key' => 'value'];
        $data = [
            'simpleKey' => $array,
            'key' => [
                'innerKey' => 'innerValue',
            ],
        ];
        // <editor-fold defaultstate="collapsed" desc="Simple key">
        yield 'Simple key' => [$data, 'simpleKey', $array];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="[key, innerKey]">
        yield '[key, innerKey]' => [$data, ['key', 'innerKey'], 'innerValue'];
        // </editor-fold>
    }

    /**
     * Data provider with ArrayAccess data for testOffsetGet test
     *
     * @return Generator<string, array{0: mixed[]|object, 1: int[]|int|string[]|string, 2: mixed}>
     */
    protected function offsetGetArrayAccessProvider(): Generator
    {
        $array = ArrayHash::from(['key' => 'value']);
        $data = [
            'simpleKey' => $array,
            'key' => [
                'innerKey' => 'innerValue',
            ],
        ];
        // <editor-fold defaultstate="collapsed" desc="AA Simple key">
        yield 'AA Simple key' => [$data, 'simpleKey', $array];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="AA  [key, innerKey]">
        yield 'AA [key, innerKey]' => [$data, ['key', 'innerKey'], 'innerValue'];
        // </editor-fold>
    }

    /**
     * Data provider with object data for testOffsetGet test
     *
     * @return Generator<string, array{0: mixed[]|object, 1: int[]|int|string[]|string, 2: mixed}>
     */
    protected function offsetGetObjectProvider(): Generator
    {
        $object = (object) ['key' => 'value'];
        $data = [
            'simpleKey' => $object,
            'objectKey' => (object) [
                'innerKey' => 'innerObjectValue',
            ],
            'arrayHashKey' => ArrayHash::from([
                'innerKey' => 'innerValue',
            ]),
        ];
        // <editor-fold defaultstate="collapsed" desc="Simple key from object">
        yield 'Simple key from object' => [(object) $data, 'simpleKey', $object];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="[objectKey, innerKey] from ArrayHash">
        yield '[objectKey, innerKey] from ArrayHash' => [
            ArrayHash::from($data), ['objectKey', 'innerKey'], 'innerObjectValue',
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="[arrayHashKey, innerKey] from object">
        yield '[arrayHashKey, innerKey] from object' => [(object) $data, ['arrayHashKey', 'innerKey'], 'innerValue'];
        // </editor-fold>
    }

    /**
     * Tester of offsetGet implementation
     *
     * @param mixed[]|object $array
     * @param int[]|int|string[]|string $key
     * @param mixed $expected
     * @return void
     * @covers ::offsetGet
     * @group integration
     * @dataProvider offsetGetArrayProvider
     * @dataProvider offsetGetArrayAccessProvider
     * @dataProvider offsetGetObjectProvider
     */
    public function testOffsetGet($array, $key, $expected): void
    {
        self::assertSame($expected, Arrays::offsetGet($array, $key));
    }

    /**
     * Tester of offsetGet implementation for default value
     *
     * @return void
     * @group integration
     */
    public function testOffsetGetDefault(): void
    {
        $default = 'default';
        self::assertSame($default, Arrays::offsetGet([], 'key', $default));
    }

    /**
     * Data provider for testOffsetGetException test
     *
     * @return Generator<string, array{0: int[]|int|string[]|string, 1: array<string, string>}>
     */
    protected function offsetGetExceptionProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="Simple key">
        yield 'Simple key' => ['simpleKey', ['item' => '[simpleKey]']];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="[arrayHashKey, innerKey]">
        yield '[arrayHashKey, innerKey]' => [['arrayHashKey', 'innerKey'], ['item' => '[arrayHashKey][innerKey]']];
        // </editor-fold>
    }

    /**
     * Tester of offsetGet implementation for exception handler
     *
     * @param int[]|int|string[]|string $key
     * @param array<string, string> $data
     * @return void
     * @covers ::offsetGet
     * @group negative
     * @dataProvider offsetGetExceptionProvider
     */
    public function testOffsetGetException($key, array $data): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Missing item :item');
        $this->expectExceptionData($data);
        Arrays::offsetGet([], $key);
    }

    /**
     * Data provider with array data testOffsetExists test
     *
     * @return Generator<string, array{0: mixed[]|object, 1: mixed[]|int|string, 2: bool}>
     */
    protected function offsetExistsArrayProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="array exists">
        yield 'array exists' => [
            ['one' => ['two' => ['three' => 4]]],
            ['one', 'two', 'three'],
            true,
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="array not exists">
        yield 'array not exists' => [
            [],
            ['one', 'two', 'three'],
            false,
        ];
        // </editor-fold>
    }

    /**
     * Data provider with ArrayAccess data testOffsetExists test
     *
     * @return Generator<string, array{0: mixed[]|object, 1: mixed[]|int|string, 2: bool}>
     */
    protected function offsetExistsArrayAccessProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="ArrayAccess exists">
        yield 'ArrayAccess exists' => [
            ArrayHash::from(['one' => ['two' => ['three' => 4]]]),
            ['one', 'two', 'three'],
            true,
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ArrayAccess not exists">
        yield 'ArrayAccess not exists' => [
            ArrayHash::from([]),
            ['one', 'two', 'three'],
            false,
        ];
        // </editor-fold>
    }

    /**
     * Data provider with object data testOffsetExists test
     *
     * @return Generator<string, array{0: mixed[]|object, 1: mixed[]|int|string, 2: bool}>
     */
    protected function offsetExistsObjectProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="object exists">
        yield 'object exists' => [
            (object) ['one' => ['two' => (object) ['three' => 4]]],
            ['one', 'two', 'three'],
            true,
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="object not exists">
        yield 'object not exists' => [
            (object) [],
            ['one', 'two', 'three'],
            false,
        ];
        // </editor-fold>
    }

    /**
     * Tester of offsetExists implementation
     *
     * @param mixed[]|object $array
     * @param int[]|int|string[]|string $key
     * @param bool $expectedResult
     * @return void
     * @covers ::offsetExists
     * @dataProvider offsetExistsArrayProvider
     * @dataProvider offsetExistsArrayAccessProvider
     * @dataProvider offsetExistsObjectProvider
     */
    public function testOffsetExists($array, $key, bool $expectedResult): void
    {
        self::assertSame($expectedResult, Arrays::offsetExists($array, $key));
    }

    /**
     * Data provider with array data for testOffsetSet test
     *
     * @return Generator<string, array{0: mixed[]|object, 1: int[]|int|string[]|string, 2: mixed, 3: mixed[]|object}>
     */
    protected function offsetSetArrayProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="array key exists">
        yield 'array key exists' => [
            ['one' => ['two' => ['three' => 4]]],
            ['one', 'two', 'three'],
            'four',
            ['one' => ['two' => ['three' => 'four']]],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="array key not exists">
        yield 'array key not exists' => [
            ['one' => null],
            ['one', 'two', 'three'],
            'four',
            ['one' => ['two' => ['three' => 'four']]],
        ];
        // </editor-fold>
    }

    /**
     * Data provider with ArrayAccess data for testOffsetSet test
     *
     * @return Generator<string, array{0: mixed[]|object, 1: int[]|int|string[]|string, 2: mixed, 3: mixed[]|object}>
     */
    protected function offsetSetArrayAccessProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="ArrayAccess key exists">
        yield 'ArrayAccess key exists' => [
            ArrayHash::from(['one' => ['two' => ['three' => 4]]]),
            ['one', 'two', 'three'],
            'four',
            ArrayHash::from(['one' => ['two' => ['three' => 'four']]]),
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ArrayAccess key not exists">
        yield 'ArrayAccess key not exists' => [
            ArrayHash::from(['one' => null]),
            ['one', 'two', 'three'],
            'four',
            ArrayHash::from(['one' => ['two' => ['three' => 'four']]]),
        ];
        // </editor-fold>
    }

    /**
     * Data provider with object data for testOffsetSet test
     *
     * @return Generator<string, array{0: mixed[]|object, 1: int[]|int|string[]|string, 2: mixed, 3: mixed[]|object}>
     */
    protected function offsetSetObjectProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="object key exists">
        yield 'object key exists' => [
            (object) ['one' => ['two' => (object) ['three' => 4]]],
            ['one', 'two', 'three'],
            'four',
            (object) ['one' => ['two' => (object) ['three' => 'four']]],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="object key not exists">
        yield 'object key not exists' => [
            (object) ['one' => null],
            ['one', 'two', 'three'],
            'four',
            (object) ['one' => (object) ['two' => (object) ['three' => 'four']]],
        ];
        // </editor-fold>
    }

    /**
     * Data provider with self data for testOffsetSet test
     *
     * @return Generator<string, array{0: mixed[]|object, 1: int[]|int|string[]|string, 2: mixed, 3: mixed[]|object}>
     */
    protected function offsetSetSelfProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="self array to object">
        yield 'self array to object' => [
            (object) ['one' => 'two'], [], ['three' => 'four'], (object) ['one' => 'two', 'three' => 'four'],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="self non-structure to object">
        yield 'self non-structure to object' => [
            (object) ['one' => 'two'], [], false, (object) ['one' => 'two', false],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="self object to array">
        yield 'self object to array' => [
            ['one' => 'two'], [], ['three' => 'four'], ['one' => 'two', 'three' => 'four'],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="self non-structure to array">
        yield 'self non-structure to array' => [
            ['one' => 'two'], [], false, ['one' => 'two', false],
        ];
        // </editor-fold>
    }

    /**
     * Tester of offsetSet implementation
     *
     * @param mixed[]|object $array
     * @param int[]|int|string[]|string $key
     * @param mixed $value
     * @param mixed[]|object $expectedArray
     * @return void
     * @covers ::offsetSet
     * @group integration
     * @dataProvider offsetSetArrayProvider
     * @dataProvider offsetSetArrayAccessProvider
     * @dataProvider offsetSetSelfProvider
     * @dataProvider offsetSetObjectProvider
     * @throws JsonException annotation
     */
    public function testOffsetSet($array, $key, $value, $expectedArray): void
    {
        Arrays::offsetSet($array, $key, $value);
        $result = Json::encode($expectedArray, JSON_PRETTY_PRINT);
        self::assertSame(Json::encode($array, JSON_PRETTY_PRINT), $result);
    }

    /**
     * Data provider for testOffsetSetException test
     *
     * @return Generator<string, array{0: mixed[]|object, 1: int[]|int|string[]|string, 2: array<string, string>}>
     */
    protected function offsetSetExceptionProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="string value">
        yield 'string value' => [
            (object) ['one' => ['two' => 'three']],
            ['one', 'two', 'three'],
            ['index' => 'three', 'type' => 'string'],
        ];
        // </editor-fold>
    }

    /**
     * Tester of offsetSet implementation for exception handler
     *
     * @param mixed[]|object $array
     * @param int[]|int|string[]|string $key
     * @param array<string, string> $data
     * @return void
     * @covers ::offsetSet
     * @group negative
     * @dataProvider offsetSetExceptionProvider
     */
    public function testOffsetSetException($array, $key, array $data): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Unsupported index ":index" type ":type"');
        $this->expectExceptionData($data);
        Arrays::offsetSet($array, $key, 'value');
    }

    /**
     * Data provider with array data for testOffsetUnset test
     *
     * @return Generator<string, array{0: mixed[]|object, 1: mixed[]|int|string, 2: mixed[]|object}>
     */
    public function offsetUnsetProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="array">
        yield 'array' => [
            ['one' => ['two' => ['three' => 4]]],
            ['one', 'two', 'three'],
            ['one' => ['two' => []]],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ArrayAccess">
        yield 'ArrayAccess' => [
            ArrayHash::from(['one' => ['two' => ['three' => 4]]]),
            ['one', 'two', 'three'],
            ArrayHash::from(['one' => ['two' => []]]),
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="object">
        yield 'object' => [
            (object) ['one' => ['two' => (object) ['three' => 4]]],
            ['one', 'two', 'three'],
            (object) ['one' => ['two' => (object) []]],
        ];
        // </editor-fold>
    }

    /**
     * Tester of offsetUnset implementation
     *
     * @param mixed[]|object $array
     * @param int[]|int|string[]|string $key
     * @param mixed[]|object $expectedArray
     * @return void
     * @covers ::offsetUnset
     * @group integration
     * @dataProvider offsetUnsetProvider
     * @throws JsonException annotation
     */
    public function testOffsetUnset($array, $key, $expectedArray): void
    {
        Arrays::offsetUnset($array, $key);
        self::assertSame(Json::encode($array, JSON_PRETTY_PRINT), Json::encode($expectedArray, JSON_PRETTY_PRINT));
    }

    // </editor-fold>
}
