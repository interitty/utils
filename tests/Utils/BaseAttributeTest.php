<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Interitty\PhpUnit\BaseTestCase;
use PHPUnit\Framework\MockObject\MockObject;
use ReflectionMethod;

/**
 * @coversDefaultClass Interitty\Utils\BaseAttribute
 */
class BaseAttributeTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of ReflectionMethod getter/setter implementation
     *
     * @return void
     * @covers ::getReflectionMethod
     * @covers ::setReflectionMethod
     */
    public function testGetSetReflectionMethod(): void
    {
        $reflectionMethod = $this->createReflectionMethodMock();
        $baseAttribute = $this->createBaseAttributeMock();
        $this->processTestGetSet($baseAttribute, 'reflectionMethod', $reflectionMethod);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * BaseAttribute mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return BaseAttribute&MockObject
     */
    protected function createBaseAttributeMock(array $methods = []): BaseAttribute&MockObject
    {
        $mock = $this->createMockAbstract(BaseAttribute::class, $methods);
        return $mock;
    }

    /**
     * ReflectionMethod mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return ReflectionMethod&MockObject
     */
    protected function createReflectionMethodMock(array $methods = []): ReflectionMethod&MockObject
    {
        $mock = $this->createPartialMock(ReflectionMethod::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
