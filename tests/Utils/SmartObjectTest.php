<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Interitty\PhpUnit\BaseTestCase;
use PHPUnit\Framework\MockObject\MockObject;

use function assert;
use function method_exists;

/**
 * @coversDefaultClass Interitty\Utils\SmartObject
 */
class SmartObjectTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of event with possible referenced parameters trigger implementation
     *
     * @return void
     * @covers ::triggerEvent
     */
    public function testTriggerEvent(): void
    {
        $eventName = 'onEvent';
        $parameters = [&$eventName];
        $notifier = $this->createSmartObjectMock(['__call']);
        $notifier->expects(self::once())
            ->method('__call')->with(self::equalTo($eventName), self::equalTo($parameters));
        assert(method_exists($notifier, 'triggerEvent'));
        $notifier->triggerEvent($eventName, $parameters);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * SmartObject mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return MockObject
     */
    protected function createSmartObjectMock(array $methods = []): MockObject
    {
        /**
         * @phpstan-var class-string $className
         * @phpstan-ignore varTag.nativeType
         */
        $className = 'SmartObjectNotifier';
        $code = '<?php
        class ' . $className . ' {
            public array $onEvent = [];

            use ' . SmartObject::class . ';
        }';
        $this->processRegisterAutoload($className, $code);
        $mock = $this->createMockAbstract($className, $methods);
        return $mock;
    }

    // </editor-fold>
}
