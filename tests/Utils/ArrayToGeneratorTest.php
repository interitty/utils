<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Generator;
use Interitty\PhpUnit\BaseTestCase;

use function iterator_to_array;

/**
 * @coversDefaultClass Interitty\Utils\Arrays
 */
class ArrayToGeneratorTest extends BaseTestCase
{
    /**
     * Tester of array to generator helper implementation
     *
     * @return void
     * @covers ::arrayToGenerator
     */
    public function testArrayToGenerator(): void
    {
        $array = [0 => 1, 'a' => 'b'];
        $generator = Arrays::arrayToGenerator($array);
        self::assertInstanceOf(Generator::class, $generator); // @phpstan-ignore-line
        self::assertSame($array, iterator_to_array($generator));
    }
}
