<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Generator;
use Interitty\PhpUnit\BaseTestCase;
use InvalidArgumentException;

/**
 * @coversDefaultClass Interitty\Utils\DateTime
 */
class DateTimeTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Data provider for processParseIso8601 without TimeZone test
     *
     * @return Generator<string, array{0: string, 1: array<string, string>}>
     */
    public function processParseIso8601Provider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="1989-12-17T12:00">
        yield '1989-12-17T12:00' => [
            '1989-12-17T12:00',
            [
                'year' => '1989',
                'month' => '12',
                'day' => '17',
                'hour' => '12',
                'minutes' => '00',
                'seconds' => '00',
                'microseconds' => '000000',
            ],
        ];
        // </editor-fold>
    }

    /**
     * Data provider for processParseIso8601 with hours TimeZone test
     *
     * @return Generator<string, array{0: string, 1: array<string, string>}>
     */
    public function processParseIso8601HoursProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="1989-12-17T12:00+00:00">
        yield '1989-12-17T12:00+00:00' => [
            '1989-12-17T12:00+00:00',
            [
                'year' => '1989',
                'month' => '12',
                'day' => '17',
                'hour' => '12',
                'minutes' => '00',
                'seconds' => '00',
                'microseconds' => '000000',
                'timeZone' => '+00:00',
                'timeZoneOperation' => '+',
                'timeZoneHours' => '00',
                'timeZoneMinutes' => '00',
            ],
        ];
        // </editor-fold>
    }

    /**
     * Data provider for processParseIso8601 with military TimeZone test
     *
     * @return Generator<string, array{0: string, 1: array<string, string>}>
     */
    public function processParseIso8601MilitaryProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="1989-12-17 12:00Z">
        yield '1989-12-17 12:00Z' => [
            '1989-12-17 12:00Z',
            [
                'year' => '1989',
                'month' => '12',
                'day' => '17',
                'hour' => '12',
                'minutes' => '00',
                'seconds' => '00',
                'microseconds' => '000000',
                'timeZone' => 'Z',
                'timeZoneMilitary' => 'Z',
            ],
        ];
        // </editor-fold>
    }

    /**
     * Tester of processParseIso8601 implementation
     *
     * @param string $isoDateTime
     * @param string[] $expectedResult
     * @return void
     * @dataProvider processParseIso8601Provider
     * @dataProvider processParseIso8601HoursProvider
     * @dataProvider processParseIso8601MilitaryProvider
     * @group integration
     * @covers ::processParseIso8601
     * @covers \Interitty\Utils\RegexPattern::getIso8601Pattern
     */
    public function testProcessParseIso8601(string $isoDateTime, array $expectedResult): void
    {
        self::assertSame($expectedResult, DateTime::processParseIso8601($isoDateTime));
    }

    /**
     * Data provider for processParseIso8601Error test
     *
     * @return Generator<string, array{0: string}>
     */
    public function processParseIso8601ErrorProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="Empty string">
        yield 'Empty string' => [
            '',
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Not datetime string">
        yield 'Not datetime string' => [
            'test',
        ];
        // </editor-fold>
    }

    /**
     * Tester of processParseIso8601 error implementation
     *
     * @param string $isoDateTime A string representing the DateTime in ISO 8601 format
     * @return void
     * @group negative
     * @dataProvider processParseIso8601ErrorProvider
     * @covers ::processParseIso8601
     */
    public function testCreateFromIso8601Error(string $isoDateTime): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Unable to parse datetime from ":dateTime", string should match ":pattern"');
        $this->expectExceptionData([
            'dateTime' => $isoDateTime,
            'pattern' => RegexPattern::getIso8601Pattern(),
        ]);
        DateTime::processParseIso8601($isoDateTime);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">

    /**
     * DateTimeFactory factory
     *
     * @return DateTimeFactory
     */
    protected function createDateTimeFactory(): DateTimeFactory
    {
        $dateTimeFactory = new DateTimeFactory();
        return $dateTimeFactory;
    }
    // </editor-fold>
}
