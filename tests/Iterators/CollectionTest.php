<?php

declare(strict_types=1);

namespace Interitty\Iterators;

use Generator;
use Interitty\PhpUnit\BaseTestCase;
use Nette\Utils\AssertionException;

use function iterator_to_array;

/**
 * @coversDefaultClass Interitty\Iterators\Collection
 */
class CollectionTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of delete integration
     *
     * @return void
     * @covers ::delete
     * @group integration
     */
    public function testDelete(): void
    {
        $deletedKey = 1;
        $data = [0 => 'test1', $deletedKey => 'test2', 2 => 'test3'];
        $expected = [0 => 'test1', 2 => 'test3'];
        $collection = $this->createCollection('string');
        $collection->addCollection($data);
        $collection->delete($deletedKey);

        self::assertSame($expected, iterator_to_array($collection));
        self::assertSameSize($expected, $collection);
    }

    /**
     * Tester of get integration for non added key
     *
     * @return void
     * @covers ::get
     * @group integration
     */
    public function testGetNotAdded(): void
    {
        $noAddedKey = 'notAddedKey';
        $data = ['test1', 'test2', 'test3'];
        $collection = $this->createCollection('string');
        $collection->addCollection($data);

        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('Item with key ":key" was not added');
        $this->expectExceptionData([
            'key' => $noAddedKey,
        ]);
        $collection->get($noAddedKey);
    }

    /**
     * Tester of collection integration
     *
     * @return void
     * @group integration
     */
    public function testIntegration(): void
    {
        $data = ['test1', 'test2', 'test3'];
        $collection = $this->createCollection('string');
        $collection->addCollection($data);

        self::assertSame($data, iterator_to_array($collection));
        self::assertSameSize($data, $collection);
    }

    // </editor-fold>
    /**
     * Tester of constructor
     *
     * @return void
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $type = 'type';
        $collection = $this->createCollection($type);
        self::assertSame($type, $collection->getType());
    }

    /**
     * Dataprovider for checkType
     *
     * @return Generator<string, array{0: string, 1: mixed}>
     */
    public function checkTypeDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="float">
        yield 'float' => ['float', 42.0];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="integer">
        yield 'integer' => ['integer', 42];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="string">
        yield 'string' => ['string', 'test'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Collection">
        yield 'Collection' => [Collection::class, new Collection('string')];
        // </editor-fold>
    }

    /**
     * Tester of checkType implementation
     *
     * @param string $type
     * @param mixed $value
     * @return void
     * @dataProvider checkTypeDataProvider
     * @covers ::checkType
     * @group integration
     */
    public function testCheckType(string $type, $value): void
    {
        $collection = $this->createCollection($type);
        $this->callNonPublicMethod($collection, 'checkType', [$value]);

        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('The variable expects to be ' . $type . ', null given.');
        $this->expectExceptionMessage('The :label expects to be :expected, :type given');
        $this->expectExceptionData(['expected' => $type, 'label' => 'variable', 'type' => 'null']);
        $this->callNonPublicMethod($collection, 'checkType', [null]);
    }

    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of Type getter/setter implementation
     *
     * @param string $type
     * @return void
     * @dataProvider stringDataProvider
     * @covers ::getType
     * @covers ::setType
     */
    public function testGetSetType(string $type): void
    {
        $this->processTestGetSet(Collection::class, 'type', $type);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Collection  factory
     *
     * @param string $type
     * @return Collection
     * @phpstan-return Collection<string>
     */
    protected function createCollection(string $type): Collection
    {
        /** @phpstan-var Collection<string> $collection */
        $collection = new Collection($type);
        return $collection;
    }

    // </editor-fold>
}
