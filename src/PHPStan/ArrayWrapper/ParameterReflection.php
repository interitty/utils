<?php

declare(strict_types=1);

namespace Interitty\PHPStan\ArrayWrapper;

use PHPStan\Reflection\ParameterReflection as PHPStanParameterReflection;
use PHPStan\Reflection\PassedByReference;
use PHPStan\Type\Type;

class ParameterReflection implements PHPStanParameterReflection
{
    /** @var string */
    protected string $name;

    /** @var Type */
    protected Type $type;

    /**
     * Constructor
     *
     * @param Type $type
     * @param string $name
     * @return void
     */
    public function __construct(Type $type, string $name)
    {
        $this->setType($type);
        $this->setName($name);
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * @inheritDoc
     */
    public function getDefaultValue(): ?Type
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'value';
    }

    /**
     * Name setter
     *
     * @param string $name
     * @return static Provides fluent interface
     */
    protected function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isOptional(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function passedByReference(): PassedByReference
    {
        return PassedByReference::createNo();
    }

    /**
     * @inheritDoc
     */
    public function getType(): Type
    {
        return $this->type;
    }

    /**
     * Type setter
     *
     * @param Type $type
     * @return static Provides fluent interface
     */
    protected function setType(Type $type): static
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isVariadic(): bool
    {
        return false;
    }

    // </editor-fold>
}
