<?php

declare(strict_types=1);

namespace Interitty\PHPStan\ArrayWrapper;

use PHPStan\Reflection\ClassMemberReflection;
use PHPStan\Reflection\ClassReflection;
use PHPStan\Reflection\FunctionVariant;
use PHPStan\Reflection\MethodReflection as PHPStanMethodReflection;
use PHPStan\Reflection\ParameterReflection;
use PHPStan\Reflection\ParametersAcceptor;
use PHPStan\TrinaryLogic;
use PHPStan\Type\Generic\TemplateTypeMap;
use PHPStan\Type\Type;

class MethodReflection implements PHPStanMethodReflection
{
    /** @var ClassReflection */
    protected ClassReflection $declaringClass;

    /** @var string */
    protected string $name;

    /** @var bool */
    protected bool $sideEffects = false;

    /** @phpstan-var list<ParametersAcceptor> */
    protected array $variants = [];

    /**
     * Constructor
     *
     * @param ClassReflection $declaringClass
     * @param string $name
     * @return void
     */
    public function __construct(ClassReflection $declaringClass, string $name)
    {
        $this->setDeclaringClass($declaringClass);
        $this->setName($name);
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * @inheritDoc
     */
    public function getDeclaringClass(): ClassReflection
    {
        return $this->declaringClass;
    }

    /**
     * DeclaringClass setter
     *
     * @param ClassReflection $declaringClass
     * @return static Provides fluent interface
     */
    protected function setDeclaringClass(ClassReflection $declaringClass): static
    {
        $this->declaringClass = $declaringClass;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isDeprecated(): TrinaryLogic
    {
        return TrinaryLogic::createNo();
    }

    /**
     * @inheritDoc
     */
    public function getDeprecatedDescription(): ?string
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getDocComment(): ?string
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function isFinal(): TrinaryLogic
    {
        return TrinaryLogic::createNo();
    }

    /**
     * @inheritDoc
     */
    public function isInternal(): TrinaryLogic
    {
        return TrinaryLogic::createNo();
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Name setter
     *
     * @param string $name
     * @return static Provides fluent interface
     */
    protected function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isPrivate(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function getPrototype(): ClassMemberReflection
    {
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isPublic(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function hasSideEffects(): TrinaryLogic
    {
        $sideEffects = TrinaryLogic::createFromBoolean($this->sideEffects);
        return $sideEffects;
    }

    /**
     * SideEffects setter
     *
     * @param bool $sideEffects [OPTIONAL]
     * @return static Provides fluent interface
     */
    public function setSideEffects(bool $sideEffects = true): static
    {
        $this->sideEffects = $sideEffects;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isStatic(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function getThrowType(): ?Type
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getVariants(): array
    {
        return $this->variants;
    }

    /**
     * Variant adder
     *
     * @param ParameterReflection[] $parameters
     * @param Type $returnType
     * @return static Provides fluent interface
     * @phpstan-param list<ParameterReflection> $parameters
     */
    public function addVariant(array $parameters, Type $returnType): static
    {
        $this->variants[] = $this->createFunctionVariant($parameters, $returnType);
        return $this;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * FunctionVariant factory
     *
     * @param ParameterReflection[] $parameters
     * @param Type $returnType
     * @return FunctionVariant
     * @phpstan-param list<ParameterReflection> $parameters
     */
    protected function createFunctionVariant(array $parameters, Type $returnType): FunctionVariant
    {
        $map = TemplateTypeMap::createEmpty();
        $functionVariant = new FunctionVariant($map, $map, $parameters, false, $returnType);
        return $functionVariant;
    }

    // </editor-fold>
}
