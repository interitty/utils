<?php

declare(strict_types=1);

namespace Interitty\PHPStan\ArrayWrapper;

use Interitty\Utils\Strings;
use LogicException;
use PHPStan\Reflection\ClassReflection;
use PHPStan\Reflection\MethodReflection as PHPStanMethodReflection;
use PHPStan\Reflection\MethodsClassReflectionExtension as PHPStanMethodsClassReflectionExtension;
use PHPStan\Type\BooleanType;
use PHPStan\Type\Constant\ConstantStringType;
use PHPStan\Type\ErrorType;
use PHPStan\Type\Type;
use PHPStan\Type\VoidType;

use function assert;
use function constant;
use function is_array;
use function is_string;

class MethodsClassReflectionExtension implements PHPStanMethodsClassReflectionExtension
{
    /** @var string */
    protected string $className;

    /**
     * Constructor
     *
     * @param string $className
     * @return void
     * @phpstan-param class-string $className
     */
    public function __construct(string $className)
    {
        $this->setClassName($className);
    }

    /**
     * @inheritDoc
     */
    public function hasMethod(ClassReflection $classReflection, string $methodName): bool
    {
        $hasMethod = false;
        $ancestor = $classReflection->getAncestorWithClassName($this->getClassName());
        if (($ancestor instanceof ClassReflection) === true) {
            /** @phpstan-var array{'offset': string}|null $match */
            $match = Strings::match($methodName, $this->getClassMagicCallPattern());
            if (is_array($match) === true) {
                $hasMethod = $this->hasOffsetValueType($ancestor, Strings::firstLower($match['offset']));
            }
        }
        return $hasMethod;
    }

    /**
     * @inheritDoc
     */
    public function getMethod(ClassReflection $classReflection, string $methodName): PHPStanMethodReflection
    {
        /** @var array{method: string, offset: string}|null $match */
        $match = Strings::match($methodName, $this->getClassMagicCallPattern());
        $ancestor = $classReflection->getAncestorWithClassName($this->getClassName());
        if ((($ancestor instanceof ClassReflection) !== true) || (is_array($match) !== true)) {
            throw new LogicException('Unsupported class or method');
        }

        $method = $this->createMethodReflection($classReflection, $methodName);
        $offsetValueType = $this->getOffsetValueType($ancestor, Strings::firstLower($match['offset']));
        if ($match['method'] === 'get') {
            $method->addVariant([], $offsetValueType);
        } elseif (($match['method'] === 'has') || ($match['method'] === 'is')) {
            $method->addVariant([], $this->createBooleanType());
        } elseif ($match['method'] === 'set') {
            $method->addVariant([$this->createParameterReflection($offsetValueType, 'value')], $this->createVoidType())
                ->setSideEffects();
        } elseif ($match['method'] === 'unset') {
            $method->addVariant([], $this->createVoidType())
                ->setSideEffects();
        }
        return $method;
    }

    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * BooleanType factory
     * @return BooleanType
     */
    protected function createBooleanType(): BooleanType
    {
        $booleanType = new BooleanType();
        return $booleanType;
    }

    /**
     * ConstantStringType factory
     *
     * @param string $value
     * @return ConstantStringType
     */
    protected function createConstantStringType(string $value): ConstantStringType
    {
        $constantStringType = new ConstantStringType($value);
        return $constantStringType;
    }

    /**
     * MethodReflection factory
     *
     * @param ClassReflection $classReflection
     * @param string $name
     * @return MethodReflection
     */
    protected function createMethodReflection(ClassReflection $classReflection, string $name): MethodReflection
    {
        $methodReflection = new MethodReflection($classReflection, $name);
        return $methodReflection;
    }

    /**
     * ParameterReflection factory
     *
     * @param Type $offsetType
     * @param string $name
     * @return ParameterReflection
     */
    protected function createParameterReflection(Type $offsetType, string $name): ParameterReflection
    {
        $parameterReflection = new ParameterReflection($offsetType, $name);
        return $parameterReflection;
    }

    /**
     * VoidType factory
     *
     * @return VoidType
     */
    protected function createVoidType(): VoidType
    {
        $voidType = new VoidType();
        return $voidType;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * MagicCallPattern getter
     *
     * @return string
     */
    protected function getClassMagicCallPattern(): string
    {
        $className = $this->getClassName();
        $constantName = $className . '::MAGIC_CALL_PATTERN';
        /**
         * @phpstan-ignore missingType.checkedException
         */
        $magicCallPattern = defined($constantName) ? constant($constantName) : null;
        assert(is_string($magicCallPattern));
        return $magicCallPattern;
    }

    /**
     * ClassName getter
     *
     * @return string
     */
    protected function getClassName(): string
    {
        return $this->className;
    }

    /**
     * ClassName setter
     *
     * @param string $className
     * @return static Provides fluent interface
     */
    protected function setClassName(string $className): static
    {
        $this->className = $className;
        return $this;
    }

    /**
     * Offset value type getter
     *
     * @param ClassReflection $ancestor
     * @param string $offset
     * @return Type
     */
    protected function getOffsetValueType(ClassReflection $ancestor, string $offset): Type
    {
        $map = $ancestor->getActiveTemplateTypeMap();
        $array = $map->getType('InnerArrayType');
        assert($array instanceof Type);
        $offsetType = $this->createConstantStringType($offset);
        return $array->getOffsetValueType($offsetType);
    }

    /**
     * Offset value type checker
     *
     * @param ClassReflection $ancestor
     * @param string $offset
     * @return bool
     */
    protected function hasOffsetValueType(ClassReflection $ancestor, string $offset): bool
    {
        $offsetValueType = $this->getOffsetValueType($ancestor, $offset);
        $hasOffsetValueType = (($offsetValueType instanceof ErrorType) === false);
        return $hasOffsetValueType;
    }

    // </editor-fold>
}
