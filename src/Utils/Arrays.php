<?php

declare(strict_types=1);

namespace Interitty\Utils;

use ArrayAccess;
use Generator;
use Interitty\Exceptions\Exceptions;
use Nette\InvalidArgumentException;
use Nette\Utils\Arrays as NetteArrays;
use RuntimeException;

use function array_merge;
use function array_shift;
use function func_num_args;
use function gettype;
use function implode;
use function is_array;
use function is_iterable;
use function is_null;
use function is_object;

/**
 * @phpstan-import-type ArrayToSort from ArraySorter
 * @phpstan-import-type ArrayToTSort from ArraySorter
 * @suppressWarnings("PHPMD.CyclomaticComplexity")
 * @suppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class Arrays extends NetteArrays
{
    /**
     * Array column helper
     * Similar to native `array_column` function with abbility to use deeper key for column or value
     *
     * @param mixed[]|iterable|object $array
     * @param int[]|int|string[]|string $columnKey
     * @param int[]|int|string[]|string|null $indexKey [OPTIONAL] Use original array key or given column value
     * @return mixed[]
     * @phpstan-param iterable<iterable<mixed>|object> $array
     */
    public static function arrayColumn($array, $columnKey, $indexKey = null): array
    {
        $result = [];
        foreach ($array as $key => $columnArray) {
            if (self::offsetExists($columnArray, $columnKey) === false) {
                continue;
            }
            $value = self::offsetGet($columnArray, $columnKey);
            if (is_null($indexKey) === false) {
                $key = self::offsetGet($columnArray, $indexKey, default: null);
            }
            if ($key === null) {
                $result[] = $value;
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    /**
     * Array to generator helper
     *
     * @param array<array-key, mixed> $array
     * @return Generator
     */
    public static function arrayToGenerator(array $array): Generator
    {
        foreach ($array as $key => $value) {
            yield $key => $value;
        }
    }

    /**
     * Item from array getter
     * If it does not exist, it throws an exception, unless a default value is set
     *
     * @param mixed[]|iterable|object $array
     * @param int[]|int|string[]|string $key
     * @param mixed $default [OPTIONAL]
     * @return mixed
     * @throws InvalidArgumentException if item does not exist and default value is not provided
     */
    public static function offsetGet($array, $key, $default = null)
    {
        $tokens = is_iterable($key) ? $key : (array) $key;
        $result = $array;
        foreach ($tokens as $part) {
            if ((is_array($result) === true) && (isset($result[$part]))) {
                $result = $result[$part];
            } elseif (($result instanceof ArrayAccess) && ($result->offsetExists($part))) {
                $result = $result->offsetGet($part);
            } elseif ((is_object($result) === true) && (isset($result->{$part}))) {
                $result = $result->{$part};
            } elseif (func_num_args() > 2) {
                $result = $default;
                break;
            } else {
                throw Exceptions::extend(InvalidArgumentException::class)
                        ->setMessage('Missing item :item')
                        ->addData('item', '[' . implode('][', $tokens) . ']');
            }
        }
        return $result;
    }

    /**
     * Item from array checker
     * If it does not exist, it throws an exception, unless a default value is set
     *
     * @param mixed[]|iterable|object|mixed $array
     * @param int[]|int|string[]|string $key
     * @return bool
     */
    public static function offsetExists($array, $key): bool
    {
        $tokens = is_iterable($key) ? $key : (array) $key;
        $part = array_shift($tokens);
        $result = false;
        if (($tokens === []) && (is_array($array) === true)) {
            $result = isset($array[$part]);
        } elseif (($tokens === []) && ($array instanceof ArrayAccess)) {
            $result = $array->offsetExists($part);
        } elseif (($tokens === []) && (is_object($array) === true)) {
            $result = isset($array->{$part});
        } elseif (is_array($array) === true) {
            $result = (isset($array[$part]) === true) ? self::offsetExists($array[$part], $tokens) : false;
        } elseif ($array instanceof ArrayAccess) {
            $result = ($array->offsetExists($part) === true) ? self::offsetExists($array[$part], $tokens) : false;
        } elseif (is_object($array) === true) {
            $result = (isset($array->{$part}) === true) ? self::offsetExists($array->{$part}, $tokens) : false;
        }
        return $result;
    }

    /**
     * Item into array setter
     *
     * @param mixed[]|iterable|object|mixed $array
     * @param int[]|int|string[]|string $key
     * @param mixed $value
     * @return void
     * @throws InvalidArgumentException if some inner part is not array or object
     */
    public static function offsetSet(&$array, $key, $value): void
    {
        $tokens = is_iterable($key) ? $key : (array) $key;
        $part = array_shift($tokens);
        if (($tokens === []) && (is_array($array) === true)) {
            ($part === null) ? $array = array_merge($array, (array) $value) : $array[$part] = $value;
        } elseif (($tokens === []) && (is_object($array) === true)) {
            ($part === null) ? Helpers::setObjectVars($array, (array) $value) : $array->{$part} = $value;
        } elseif ((is_array($array) === true)) {
            if (isset($array[$part]) === false) {
                $array[$part] = [];
            }
            self::offsetSet($array[$part], $tokens, $value);
        } elseif (is_object($array) === true) {
            if (isset($array->{$part}) === false) {
                $array->{$part} = (object) [];
            }
            self::offsetSet($array->{$part}, $tokens, $value);
        } else {
            throw Exceptions::extend(new InvalidArgumentException('Unsupported index ":index" type ":type"'))
                    ->setData(['index' => $part, 'type' => gettype($array)]);
        }
    }

    /**
     * Item from array unsetter
     *
     * @param mixed[]|iterable|object|mixed $array
     * @param int[]|int|string[]|string $key
     * @return void
     */
    public static function offsetUnset(&$array, $key): void
    {
        $tokens = is_iterable($key) ? $key : (array) $key;
        $part = array_shift($tokens);
        if (($tokens === []) && (is_array($array) === true)) {
            unset($array[$part]);
        } elseif (($tokens === []) && ($array instanceof ArrayAccess)) {
            $array->offsetUnset($part);
        } elseif (($tokens === []) && (is_object($array) === true)) {
            unset($array->{$part});
        } elseif ((is_array($array) === true) || ($array instanceof ArrayAccess) && (isset($array[$part]) === true)) {
            self::offsetUnset($array[$part], $tokens);
        } elseif ((is_object($array) === true) && (isset($array->{$part}) === true)) {
            self::offsetUnset($array->{$part}, $tokens);
        }
    }

    /**
     * Sort given array by nested value descending
     *
     * @param array[]|iterable[] $array
     * @param int[]|int|string[]|string $sortKey
     * @param ArraySorter::FALLBACK_* $fallback
     * @return array<array-key, iterable|object>
     * @throws RuntimeException
     * @template ArrayToSortTemplate of ArrayToSort
     * @phpstan-param ArrayToSortTemplate $array
     * @phpstan-return array<key-of<ArrayToSortTemplate>, value-of<ArrayToSortTemplate>>
     */
    public static function rsort($array, $sortKey, string $fallback = ArraySorter::FALLBACK_BOTTOM): array
    {
        return ArraySorter::rsort($array, $sortKey, $fallback);
    }

    /**
     * Sort given array by nested value ascending
     *
     * @param array[]|iterable[] $array
     * @param int[]|int|string[]|string $sortKey
     * @param ArraySorter::FALLBACK_* $fallback
     * @return array<array-key, iterable|object>
     * @throws RuntimeException
     * @template ArrayToSortTemplate of ArrayToSort
     * @phpstan-param ArrayToSortTemplate $array
     * @phpstan-return array<key-of<ArrayToSortTemplate>, value-of<ArrayToSortTemplate>>
     */
    public static function sort($array, $sortKey, string $fallback = ArraySorter::FALLBACK_BOTTOM): array
    {
        return ArraySorter::sort($array, $sortKey, $fallback);
    }

    /**
     * Topological sort
     *
     * @param mixed[]|iterable $array
     * @param array[] $befores [OPTIONAL]
     * @param array[] $afters [OPTIONAL]
     * @return mixed[]
     * @throws RuntimeException
     * @template ArrayToTSortTemplate of ArrayToTSort
     * @phpstan-param ArrayToTSortTemplate $array
     * @phpstan-param array<array-key, array<array-key>> $befores
     * @phpstan-param array<array-key, array<array-key>> $afters
     * @phpstan-return array<key-of<ArrayToTSortTemplate>, value-of<ArrayToTSortTemplate>>
     */
    public static function tsort($array, array $befores = [], array $afters = []): array
    {
        return ArraySorter::tsort($array, $befores, $afters);
    }
}
