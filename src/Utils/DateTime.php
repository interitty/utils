<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Interitty\Exceptions\Exceptions;
use InvalidArgumentException;
use Nette\Utils\DateTime as NetteDateTime;

use function array_key_exists;
use function is_array;

use const PREG_UNMATCHED_AS_NULL;

/**
 * @phpstan-type iso8601 array{
 *      'day': string,
 *      'hour': string,
 *      'microseconds': string,
 *      'minutes': string,
 *      'month': string,
 *      'seconds': string,
 *      'timeZone'?: string,
 *      'timeZoneHours'?: string,
 *      'timeZoneMilitary'?: string,
 *      'timeZoneMinutes'?: string,
 *      'timeZoneOperation'?: string,
 *      'year': string|null,
 *  }
 */
class DateTime extends NetteDateTime
{
    /** @var string like RFC3339_EXTENDED format with 6 microseconds (example: 2005-08-15T15:52:01.000000+00:00) */
    public const string ATOM_EXTENDED = "Y-m-d\TH:i:s.uP";

    /** @var string like ISO8601 format with 6 microseconds (example: 2005-08-15T15:52:01.000000+0000) */
    public const string ISO8601_EXTENDED = "Y-m-d\TH:i:s.uO";

    /**
     * @phpstan-var array{
     *      'day': string|null,
     *      'hour': string|null,
     *      'microseconds': string,
     *      'minutes': string|null,
     *      'month': string|null,
     *      'seconds': string,
     *      'timeZone'?: string|null,
     *      'timeZoneHours'?: string|null,
     *      'timeZoneMilitary'?: string|null,
     *      'timeZoneMinutes'?: string|null,
     *      'timeZoneOperation'?: string|null,
     *      'year': string|null,
     *  }
     */
    protected static array $defaultIso8601 = [
        'year' => null,
        'month' => null,
        'day' => null,
        'hour' => null,
        'minutes' => null,
        'seconds' => '00',
        'microseconds' => '000000',
        'timeZone' => null,
        'timeZoneOperation' => null,
        'timeZoneHours' => null,
        'timeZoneMinutes' => null,
        'timeZoneMilitary' => null,
    ];

    /**
     * Parse ISO 8601 processor
     *
     * @param string $isoDateTime
     * @phpstan-return iso8601
     */
    public static function processParseIso8601(string $isoDateTime): array
    {
        $pattern = RegexPattern::getIso8601Pattern();
        /** @phpstan-var iso8601|null $matches */
        $matches = Strings::match($isoDateTime, $pattern, PREG_UNMATCHED_AS_NULL);
        if (is_array($matches) !== true) {
            throw Exceptions::extend(InvalidArgumentException::class)
                    ->setMessage('Unable to parse datetime from ":dateTime", string should match ":pattern"')
                    ->addData('dateTime', $isoDateTime)
                    ->addData('pattern', $pattern);
        }
        $iso8601 = [];
        foreach (self::$defaultIso8601 as $key => $value) {
            if ((array_key_exists($key, $matches) === true) && ($matches[$key] !== null)) {
                $iso8601[$key] = $matches[$key];
            } elseif ($value !== null) {
                $iso8601[$key] = $value;
            }
        }
        /**
         * @phpstan-var iso8601 $iso8601
         * @phpstan-ignore varTag.nativeType
         */
        return $iso8601;
    }
}
