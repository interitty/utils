<?php

declare(strict_types=1);

namespace Interitty\Utils;

use ArrayAccess;
use Interitty\Exceptions\Exceptions;
use RuntimeException;
use Traversable;

use function array_diff_key;
use function array_filter;
use function call_user_func_array;
use function iterator_count;
use function uksort;

/**
 * @phpstan-type MixedIterable iterable<array-key, mixed>
 * @phpstan-type ArrayToSort array<array-key, MixedIterable|object>|(
 *      ArrayAccess<array-key, iterable|object>&iterable<array-key, MixedIterable|object>
 * )
 * @phpstan-type ArrayToTSort array<array-key, mixed>|(ArrayAccess<array-key, mixed>&MixedIterable)
 * @phpstan-type UKSortCallable callable(array-key, array-key, mixed[], self::FALLBACK_*, array<array-key, bool>): ?int
 */
class ArraySorter
{
    /** All available fallback constants */
    public const string FALLBACK_BOTTOM = 'bottom';
    public const string FALLBACK_KEEP = 'keep';
    public const string FALLBACK_REMOVE = 'remove';
    public const string FALLBACK_TOP = 'top';

    /**
     * Sort given array by nested value descending
     *
     * @phpstan-param ArrayToSortTemplate $array
     * @param int[]|int|string[]|string $sortKey
     * @param self::FALLBACK_* $fallback
     * @phpstan-return array<key-of<ArrayToSortTemplate>, value-of<ArrayToSortTemplate>>
     * @throws RuntimeException
     * @template ArrayToSortTemplate of ArrayToSort
     */
    public static function rsort($array, $sortKey, string $fallback = self::FALLBACK_BOTTOM): array
    {
        $reverseSort = static function ($aKey, $bKey, array $sortValues, string $fallback, array &$remove = []): int {
            if ($fallback === self::FALLBACK_BOTTOM) {
                $fallback = self::FALLBACK_TOP;
            } elseif (($fallback === self::FALLBACK_TOP)) {
                $fallback = self::FALLBACK_BOTTOM;
            }
            /**
             * @phpstan-var array-key $aKey
             * @phpstan-var array-key $bKey
             * @phpstan-var self::FALLBACK_* $fallback
             * @phpstan-var array<array-key, bool> $remove
             */
            $result = self::processSort($aKey, $bKey, $sortValues, $fallback, $remove) * -1;
            return $result;
        };

        self::uksort($array, $sortKey, $reverseSort, $fallback);
        return $array;
    }

    /**
     * Sort given array by nested value ascending
     *
     * @phpstan-param ArrayToSortTemplate $array
     * @param int[]|int|string[]|string $sortKey
     * @param self::FALLBACK_* $fallback
     * @phpstan-return array<key-of<ArrayToSortTemplate>, value-of<ArrayToSortTemplate>>
     * @throws RuntimeException
     * @template ArrayToSortTemplate of ArrayToSort
     */
    public static function sort($array, $sortKey, string $fallback = self::FALLBACK_BOTTOM): array
    {
        self::uksort($array, $sortKey, self::processSort(...), $fallback);
        return $array;
    }

    /**
     * Topological sort
     *
     * @phpstan-param ArrayToTSortTemplate $array
     * @phpstan-param array<array-key, array<array-key>> $befores [OPTIONAL]
     * @phpstan-param array<array-key, array<array-key>> $afters [OPTIONAL]
     * @phpstan-return array<key-of<ArrayToTSortTemplate>, value-of<ArrayToTSortTemplate>>
     * @throws RuntimeException
     * @template ArrayToTSortTemplate of ArrayToTSort
     */
    public static function tsort($array, array $befores = [], array $afters = []): array
    {
        // Normalize $befores -> $afters
        foreach ($befores as $key => $values) {
            foreach ($values as $value) {
                $afters[$value][] = $key;
            }
        }

        $sorted = [];
        while (($array instanceof Traversable) ? iterator_count($array) > 0 : $array !== []) {
            /** @phpstan-var ArrayToTSortTemplate $array */
            $sorted += self::processTsort($array, $afters);
        }
        return $sorted;
    }

    /**
     * User defined comparison function sort an array by keys processor
     *
     * @phpstan-param ArrayToSortTemplate $array
     * @phpstan-param-out array<key-of<ArrayToSortTemplate>, value-of<ArrayToSortTemplate>> $array
     * @param int[]|int|string[]|string $sortKey
     * @phpstan-param UKSortCallable $callback
     * @param mixed $arguments [OPTIONAL] Pasted to callback
     * @return void
     * @template ArrayToSortTemplate of ArrayToSort
     */
    public static function uksort(&$array, $sortKey, callable $callback, ...$arguments): void
    {
        $sortValues = Arrays::arrayColumn($array, $sortKey);
        $remove = [];
        /** @phpstan-var array<array-key, mixed[]> $array */
        uksort($array, static function ($aKey, $bKey) use ($arguments, $callback, &$remove, $sortValues): int {
            /** @phpstan-var int $result */
            $result = call_user_func_array($callback, [$aKey, $bKey, $sortValues, ...$arguments, &$remove]);
            return $result;
        });
        /** @phpstan-var array<array-key, bool> $remove */
        if ($remove !== []) {
            /**
             * @phpstan-var array<key-of<ArrayToSortTemplate>, value-of<ArrayToSortTemplate>> $array
             * @phpstan-ignore varTag.type
             */
            $array = array_diff_key($array, array_filter($remove));
        }
    }

    /**
     * One iteration of sort processor
     *
     * @phpstan-param array-key $aKey
     * @phpstan-param array-key $bKey
     * @param mixed[] $sortValues
     * @param self::FALLBACK_* $fallback
     * @phpstan-param array<array-key, bool> $remove [OPTIONAL]
     * @return int
     */
    protected static function processSort(
        $aKey,
        $bKey,
        array $sortValues,
        string $fallback,
        array &$remove = []
    ): int {
        $result = match ($fallback) {
            self::FALLBACK_BOTTOM => -1,
            self::FALLBACK_TOP => 1,
            default => 0,
        };
        if (isset($sortValues[$aKey]) && isset($sortValues[$bKey])) {
            $result = ($sortValues[$aKey] <=> $sortValues[$bKey]);
        } elseif ($fallback === self::FALLBACK_REMOVE) {
            $remove[$aKey] = (isset($sortValues[$aKey]) === false);
            $remove[$bKey] = (isset($sortValues[$bKey]) === false);
            $result = ((int) isset($sortValues[$aKey])) - ((int) isset($sortValues[$bKey]));
        } elseif (($fallback !== self::FALLBACK_KEEP) && (isset($sortValues[$aKey]) || isset($sortValues[$bKey]))) {
            $result = (((int) isset($sortValues[$aKey])) - ((int) isset($sortValues[$bKey])));
            if ($fallback === self::FALLBACK_BOTTOM) {
                $result = $result * -1;
            }
        }
        return $result;
    }

    /**
     * One iteration of topological sort processor
     *
     * @phpstan-param ArrayToTSortTemplate $array
     * @phpstan-param array<array-key, array<array-key>> $afters
     * @phpstan-return array<key-of<ArrayToTSortTemplate>, value-of<ArrayToTSortTemplate>>
     * @throws RuntimeException
     * @template ArrayToTSortTemplate of ArrayToTSort
     */
    protected static function processTsort(&$array, array &$afters = []): array
    {
        $sorted = [];
        foreach ($array as $key => $value) {
            foreach ($afters[$key] ?? [] as $afterKey => $neighbor) {
                if ((isset($sorted[$neighbor]) === true) || (isset($array[$neighbor]) === false)) {
                    unset($afters[$key][$afterKey]);
                    continue;
                }
            }
            if (($afters[$key] ?? []) === []) {
                unset($afters[$key], $array[$key]); // @phpstan-ignore parameterByRef.type
                $sorted[$key] = $value;
            }
        }
        if ($sorted === []) {
            throw Exceptions::extend(new RuntimeException('Cycle found in tsort'))->addData('restKeys', $afters);
        }

        return $sorted;
    }
}
