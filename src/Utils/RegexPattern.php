<?php

declare(strict_types=1);

namespace Interitty\Utils;

class RegexPattern
{
    /**
     * ISO 8601 regular expression pattern getter
     *
     * @return string
     */
    public static function getIso8601Pattern(): string
    {
        $pattern = '~^'
            . '(?P<year>[0-9]{4})([-]?)(?P<month>[0-9]{2})\2(?P<day>[0-9]{2})'
            . '(?:[ T])'
            . '(?P<hour>[0-9]{2})([:]?)(?P<minutes>[0-9]{2})'
            . '(?:\6(?P<seconds>[0-9]{2}))?'
            . '(?:(?:[.])?(?P<microseconds>[0-9]+))?'
            . '(?:[ ])?(?P<timeZone>'
            . '(?P<timeZoneOperation>[+-])(?P<timeZoneHours>[0-9]{2})(?:[:])?(?P<timeZoneMinutes>[0-9]{2})'
            . '|'
            . '(?P<timeZoneMilitary>[A-Z])'
            . ')?'
            . '$~';
        return $pattern;
    }
}
