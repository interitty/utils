<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Generator;
use Interitty\Exceptions\Exceptions;
use ReflectionAttribute;
use ReflectionException;
use ReflectionMethod;
use ReflectionObject as PhpReflectionObject;
use ReflectionProperty;

class ReflectionObject extends PhpReflectionObject
{
    /**
     * ReflectionProperty for a class's property getter
     *
     * @param string $propertyName
     * @param bool $deep [OPTIONAL] Search also in parents structure?
     * @return ReflectionProperty
     */
    public function getProperty(string $propertyName, bool $deep = false): ReflectionProperty
    {
        $property = null;
        if ($this->hasProperty($propertyName) === true) {
            $property = parent::getProperty($propertyName);
        } elseif ($deep === true) {
            // This is needed for private parent properties only.
            $parent = $this->getParentClass();
            while (($property === null) && ($parent !== false)) {
                if ($parent->hasProperty($propertyName) === true) {
                    $property = $parent->getProperty($propertyName);
                }

                $parent = $parent->getParentClass();
            }
        }
        if (($property instanceof ReflectionProperty) !== true) {
            throw Exceptions::extend(ReflectionException::class)
                    ->setMessage('Property :class::$:property does not exist')
                    ->addData('class', $this->getName())
                    ->addData('property', $propertyName);
        }
        return $property;
    }

    /**
     * Methods attributes getter
     *
     * @param object $object
     * @param string $attributeClass
     * @return Generator
     * @template AttributeClass of object
     * @phpstan-param class-string<AttributeClass> $attributeClass
     * @phpstan-return Generator<AttributeClass>
     */
    public static function getMethodsAttributes(object $object, string $attributeClass): Generator
    {
        $reflection = new self($object);
        /** @var ReflectionMethod $reflectionMethod */
        foreach ($reflection->getMethods() as $reflectionMethod) {
            /** @var ReflectionAttribute<AttributeClass> $reflectionAttribute */
            foreach ($reflectionMethod->getAttributes($attributeClass) as $reflectionAttribute) {
                /** @phpstan-var AttributeClass $attribute */
                $attribute = $reflectionAttribute->newInstance();
                if ($attribute instanceof BaseAttribute) {
                    $attribute->setReflectionMethod($reflectionMethod);
                }
                yield $attribute;
            }
        }
    }

    /**
     * Non-public property value getter
     *
     * @param object $object
     * @param string $propertyName
     * @return mixed Depend on the property value
     */
    public static function getNonPublicPropertyValue(object $object, string $propertyName): mixed
    {
        $reflection = new self($object);
        $property = $reflection->getProperty($propertyName, true);
        $property->setAccessible(true);
        return $property->getValue($object);
    }

    /**
     * Non-public property value setter
     *
     * @param object $object
     * @param string $propertyName
     * @param mixed $value
     * @return void
     */
    public static function setNonPublicPropertyValue(object $object, string $propertyName, mixed $value): void
    {
        $reflection = new self($object);
        $property = $reflection->getProperty($propertyName, true);
        $property->setAccessible(true);
        $property->setValue($object, $value);
    }
}
