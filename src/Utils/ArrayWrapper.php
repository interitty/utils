<?php

declare(strict_types=1);

namespace Interitty\Utils;

use ArrayAccess;
use BadMethodCallException;
use Countable;
use Interitty\Exceptions\Exceptions;
use Nette\InvalidArgumentException;

use function array_key_exists;
use function count;
use function func_num_args;
use function is_array;

/**
 * @template InnerArrayType of array<int|string, mixed>
 * @implements ArrayAccess<key-of<InnerArrayType>, value-of<InnerArrayType>>
 * @suppressWarnings("PHPMD.TooManyPublicMethods")
 */
class ArrayWrapper implements ArrayAccess, Countable
{
    /** All available regular expression pattern constants */
    public const string MAGIC_CALL_PATTERN = '~^(?P<method>(?:get|has|is|set|unset))(?P<offset>.*)$~';

    /** @phpstan-var InnerArrayType */
    protected array $array;

    /**
     * Constructor
     *
     * @phpstan-param InnerArrayType $array
     * @return void
     */
    public function __construct(array $array)
    {
        $this->setArray($array);
    }

    /**
     * Static factory
     *
     * @phpstan-param InnerArray $array [OPTIONAL]
     * @phpstan-return static<InnerArray>
     * @template InnerArray of array
     */
    public static function create(array $array = [])
    {
        $arrayWrapper = new self($array);
        return $arrayWrapper;
    }

    /**
     * Helper that allows to access array properties by the magic getters and setters
     *
     * @param string $name
     * @param mixed[] $arguments
     * @return mixed
     */
    public function __call(string $name, array $arguments)
    {
        /** @var array{method: string, offset: string}|null $match */
        $match = Strings::match($name, self::MAGIC_CALL_PATTERN);
        if (is_array($match) === true) {
            $offset = Strings::firstLower($match['offset']);
            $return = null;
            if ($match['method'] === 'get') {
                $return = $this->offsetGet($offset);
            } elseif (($match['method'] === 'has') || ($match['method'] === 'is')) {
                $return = $this->offsetExists($offset);
            } elseif ($match['method'] === 'set') {
                $this->offsetSet($offset, $arguments[0]);
            } elseif ($match['method'] === 'unset') {
                $this->offsetUnset($offset);
            }
            return $return;
        }
        throw Exceptions::extend(BadMethodCallException::class)
                ->setMessage('Call to undefined method :class:::method()')
                ->addData('class', __CLASS__)
                ->addData('method', $name);
    }

    /**
     * Array property generic getter
     *
     * @param string $name
     * @phpstan-return value-of<InnerArrayType>
     */
    public function __get(string $name): mixed
    {
        return $this->array[$name];
    }

    /**
     * Array property generic checker
     *
     * @param string $name
     * @return bool
     */
    public function __isset(string $name): bool
    {
        return array_key_exists($name, $this->array);
    }

    /**
     * Array property generic setter
     *
     * @param string $name
     * @phpstan-param value-of<InnerArrayType> $value
     * @return void
     */
    public function __set(string $name, mixed $value): void
    {
        $this->array[$name] = $value; // @phpstan-ignore assign.propertyType
    }

    /**
     * Array property generic unsetter
     *
     * @param string $name
     * @return void
     */
    public function __unset(string $name): void
    {
        unset($this->array[$name]);
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count($this->array);
    }

    /**
     * Array offset checker
     *
     * @phpstan-param key-of<InnerArrayType>|array-key|array-key[] $offset
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        if (is_array($offset) === true) {
            $result = Arrays::offsetExists($this, $offset);
        } else {
            $result = $this->__isset((string) $offset);
        }
        return $result;
    }

    /**
     * Array offset getter
     *
     * @phpstan-param key-of<InnerArrayType>|array-key|array-key[] $offset
     * @param mixed $default [OPTIONAL]
     * @phpstan-return value-of<InnerArrayType>|mixed
     */
    public function offsetGet($offset, $default = null): mixed
    {
        if (is_array($offset) === true) {
            $result = Arrays::offsetGet($this, $offset, $default);
        } elseif ($this->__isset((string) $offset) === true) {
            $result = $this->__get((string) $offset);
        } elseif (func_num_args() > 1) {
            $result = $default;
        } else {
            throw Exceptions::extend(InvalidArgumentException::class)
                    ->setMessage('Missing item ":item"')
                    ->addData('item', '[' . $offset . ']');
        }
        return $result;
    }

    /**
     * Array offset setter
     *
     * @phpstan-param key-of<InnerArrayType>|array-key|array-key[]|null $offset
     * @phpstan-param value-of<InnerArrayType>|mixed $value
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        if (is_array($offset) === true) {
            Arrays::offsetSet($this, $offset, $value);
        } else {
            $this->__set((string) $offset, $value);
        }
    }

    /**
     * Array offset unsetter
     *
     * @phpstan-param key-of<InnerArrayType>|array-key|array-key[] $offset
     * @return void
     */
    public function offsetUnset($offset): void
    {
        if (is_array($offset) === true) {
            Arrays::offsetUnset($this, $offset);
        } else {
            $this->__unset((string) $offset);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Array getter
     *
     * @phpstan-return InnerArrayType
     */
    public function getArray(): array
    {
        return $this->array;
    }

    /**
     * Array setter
     *
     * @phpstan-param InnerArrayType $array
     * @return static Provides fluent interface
     */
    protected function setArray(array $array)
    {
        $this->array = $array;
        return $this;
    }

    // </editor-fold>
}
