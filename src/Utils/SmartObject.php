<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Nette\SmartObject as NetteSmartObject;

trait SmartObject
{
    use NetteSmartObject;

    /**
     * Event with possible referenced parameters trigger
     *
     * @param string $eventName
     * @param array<mixed> $parameters [OPTIONAL]
     * @return void
     */
    public function triggerEvent(string $eventName, array $parameters = []): void
    {
        $this->__call($eventName, $parameters);
    }
}
