<?php

declare(strict_types=1);

namespace Interitty\Utils;

use DateTimeZone;
use Interitty\Exceptions\Exceptions;
use InvalidArgumentException;
use Throwable;

use function date_default_timezone_get;
use function is_string;
use function rtrim;
use function sprintf;

class DateTimeFactory implements DateTimeFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function create(string $time = 'now', $timeZone = null): DateTime
    {
        try {
            $dateTimeZone = $this->createTimeZone($timeZone);
            $dateTime = new DateTime($time, $dateTimeZone);
            return $dateTime;
        } catch (Throwable $exception) {
            throw Exceptions::extend(InvalidArgumentException::class)
                    ->setMessage($exception->getMessage())
                    ->setCode($exception->getCode())
                    ->setPrevious($exception);
        }
    }

    /**
     * @inheritdoc
     */
    public function createFromFormat(string $format, string $time, $timeZone = null): DateTime
    {
        $dateTimeZone = $this->createTimeZone($timeZone);
        $dateTime = DateTime::createFromFormat($format, $time, $dateTimeZone);
        if ($dateTime === false) {
            throw Exceptions::extend(InvalidArgumentException::class)
                    ->setMessage('Unable to create datetime object from ":dateTime", expected format is ":format"')
                    ->addData('dateTime', $time)
                    ->addData('format', $format);
        }
        return $dateTime;
    }

    /**
     * @inheritdoc
     */
    public function createFromIso8601(string $isoDateTime, $timeZone = null): DateTime
    {
        $data = DateTime::processParseIso8601($isoDateTime);
        $format = DateTime::ATOM_EXTENDED;
        $datePart = sprintf('%s-%s-%s', $data['year'], $data['month'], $data['day']);
        $timePart = sprintf('%s:%s:%s.%s', $data['hour'], $data['minutes'], $data['seconds'], $data['microseconds']);
        $time = sprintf('%sT%s', $datePart, $timePart);
        if (isset($data['timeZone']) === true) {
            $time .= $data['timeZone'];
        } else {
            $format = rtrim($format, 'P');
        }
        $dateTime = $this->createFromFormat($format, $time, $timeZone);

        if ($dateTime->format('Y-m-d') !== $datePart) {
            throw Exceptions::extend(InvalidArgumentException::class)
                    ->setMessage('Unable to create datetime object from ":dateTime", expected format is ":format"')
                    ->addData('dateTime', $isoDateTime)
                    ->addData('format', $format);
        }

        return $dateTime;
    }

    /**
     * @inheritdoc
     */
    public function createTimeZone($timeZone = null): DateTimeZone
    {
        if ($timeZone === null) {
            $timeZone = new DateTimeZone(date_default_timezone_get());
        } elseif (is_string($timeZone)) {
            $timeZone = new DateTimeZone($timeZone);
        }
        return $timeZone;
    }
}
