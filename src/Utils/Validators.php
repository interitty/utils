<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Interitty\Exceptions\Exceptions;
use Nette\Utils\AssertionException;
use Nette\Utils\Validators as NetteValidators;

use function filter_var;
use function in_array;
use function is_array;

use const FILTER_DEFAULT;

class Validators extends NetteValidators
{
    /** @var array<string,?callable> */
    protected static $validators = [
        // PHP types
        'array' => 'is_array',
        'bool' => 'is_bool',
        'boolean' => 'is_bool',
        'float' => 'is_float',
        'int' => 'is_int',
        'integer' => 'is_int',
        'null' => 'is_null',
        'object' => 'is_object',
        'resource' => 'is_resource',
        'scalar' => 'is_scalar',
        'string' => 'is_string',
        // pseudo-types
        'callable' => [__CLASS__, 'isCallable'],
        'iterable' => 'is_iterable',
        'list' => [Arrays::class, 'isList'],
        'mixed' => [__CLASS__, 'isMixed'],
        'none' => [__CLASS__, 'isNone'],
        'number' => [__CLASS__, 'isNumber'],
        'numeric' => [__CLASS__, 'isNumeric'],
        'numericint' => [__CLASS__, 'isNumericInt'],
        // string patterns
        'alnum' => 'ctype_alnum',
        'alpha' => 'ctype_alpha',
        'digit' => 'ctype_digit',
        'lower' => 'ctype_lower',
        'pattern' => null,
        'space' => 'ctype_space',
        'unicode' => [__CLASS__, 'isUnicode'],
        'upper' => 'ctype_upper',
        'xdigit' => 'ctype_xdigit',
        // syntax validation
        'email' => [__CLASS__, 'isEmail'],
        'identifier' => [__CLASS__, 'isPhpIdentifier'],
        'uri' => [__CLASS__, 'isUri'],
        'url' => [__CLASS__, 'isUrl'],
        // environment validation
        'class' => 'class_exists',
        'interface' => 'interface_exists',
        'directory' => 'is_dir',
        'file' => 'is_file',
        'type' => [__CLASS__, 'isType'],
        // files
        'executable' => 'is_executable',
        'readable' => 'is_readable',
        'writable' => 'is_writable',
        // bool
        'false' => [__CLASS__, 'isFalse'],
        'initialized' => [__CLASS__, 'isTrue'],
        'true' => [__CLASS__, 'isTrue'],
        'uninitialized' => [__CLASS__, 'isFalse'],
    ];

    /**
     * @inheritDoc
     */
    public static function assert(mixed $value, string $expected, string $label = 'variable'): void
    {
        try {
            parent::assert($value, $expected, $label);
        } catch (AssertionException $assertException) {
            $type = Strings::replace($assertException->getMessage(), '~(.*), (.*) given.~', '$2');
            if ($type === 'bool') {
                /** @phpstan-var bool $value */
                $type = $value ? 'bool true' : 'bool false';
            }
            $exception = Exceptions::extend(AssertionException::class)
                ->setMessage('The :label expects to be :expected')
                ->addData('expected', $expected)
                ->addData('label', $label);
            if (in_array($expected, ['initialized', 'uninitialized'], true) === false) {
                $exception->addMessage(', :type given')->addData('type', $type);
            }
            throw $exception;
        }
    }

    /**
     * Throws exception if a variable is of unexpected type
     *
     * @param mixed $value
     * @param string $expected Expected types separated by pipe
     * @param string $label [OPTIONAL]
     * @return bool
     */
    public static function check($value, $expected, $label = 'variable'): bool
    {
        self::assert($value, $expected, $label);
        return true;
    }

    /**
     * Throws exception if an array field is missing or of unexpected type
     *
     * @param mixed[] $array
     * @param string $field Item
     * @param string|null $expected [OPTIONAL] Expected types separated by pipe
     * @param string $label [OPTIONAL]
     * @return bool
     */
    public static function checkField($array, $field, $expected = null, $label = 'item "%" in array'): bool
    {
        self::assertField($array, $field, $expected, $label);
        return true;
    }

    /**
     * Filter given value with a specified filter
     *
     * @param mixed $value
     * @param int $filter [OPTIONAL]
     * @param mixed $options [OPTIONAL] Default value orspecific options for the filter
     * @param int|int[] $flags [OPTIONAL]
     * @return mixed
     * @see https://secure.php.net/manual/en/function.filter-var.php PHP's `filter_var()` function
     * @see https://secure.php.net/manual/en/filter.filters.validate.php Validate filters
     * @see https://secure.php.net/manual/en/filter.filters.sanitize.php Sanitize filters
     * @phpstan-template Value
     * @phpstan-template DefaultValue
     * @phpstan-param Value $value
     * @phpstan-param DefaultValue|array{'default': DefaultValue} $options
     * @phpstan-return Value|DefaultValue|false|null
     */
    public static function filterVariable(
        mixed $value,
        int $filter = FILTER_DEFAULT,
        mixed $options = null,
        int|array $flags = null
    ): mixed {
        $defaultValue = null;
        if (is_array($options) === true) {
            $defaultValue = isset($options['default']) ? $options['default'] : null;
        } elseif ($options !== null) {
            $defaultValue = $options;
            $options = ['default' => $defaultValue];
        }

        if ($value === null) {
            return $defaultValue;
        }

        $filterOptions = ['options' => $options, 'flags' => 0];
        foreach ((array) $flags as $flag) {
            $filterOptions['flags'] |= $flag;
        }

        $result = filter_var($value, $filter, $filterOptions);
        return $result;
    }

    // <editor-fold defaultstate="collapsed" desc="Checkers">
    /**
     * Checks if the value is false
     *
     * @phpstan-return ($value is false ? true : false)
     */
    public static function isFalse(mixed $value): bool
    {
        return $value === false;
    }

    /**
     * Checks if the value is true
     *
     * @phpstan-return ($value is true ? true : false)
     */
    public static function isTrue(mixed $value): bool
    {
        return $value === true;
    }

    // </editor-fold>
}
