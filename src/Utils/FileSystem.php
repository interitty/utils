<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Nette\InvalidStateException;
use Nette\IOException;
use Nette\StaticClass;
use Nette\Utils\FileSystem as NetteFileSystem; // @phpstan-ignore-line
use Nette\Utils\Random;

use function file_exists;
use function implode;
use function is_writable;

use const DIRECTORY_SEPARATOR;

class FileSystem
{
    use StaticClass;

    /**
     * Copies a file or a directory. Overwrites existing files and directories by default.
     *
     * @param string $original
     * @param string $target
     * @param bool $overwrite [OPTIONAL] Default value is true
     * @return void
     * @throws IOException
     * @throws InvalidStateException If $overwrite is set to false and destination already exists
     */
    public static function copy(string $original, string $target, bool $overwrite = true): void
    {
        NetteFileSystem::copy($original, $target, $overwrite); // @phpstan-ignore-line
    }

    /**
     * Creates a directory if it doesn't exist.
     *
     * @param string $directory
     * @param int $mode [OPTIONAL] Default value is 0777
     * @return void
     * @throws IOException
     */
    public static function createDir(string $directory, int $mode = 0777): void
    {
        NetteFileSystem::createDir($directory, $mode); // @phpstan-ignore-line
    }

    /**
     * Deletes a file or directory if exists.
     *
     * @param string $path
     * @return void
     * @throws IOException
     */
    public static function delete(string $path): void
    {
        NetteFileSystem::delete($path); // @phpstan-ignore-line
    }

    /**
     * Determines if the path is absolute.
     *
     * @param string $path
     * @return bool
     */
    public static function isAbsolute(string $path): bool
    {
        return NetteFileSystem::isAbsolute($path); // @phpstan-ignore-line
    }

    /**
     * Joins all segments of the path and normalizes the result.
     *
     * @param string $paths, ...
     * @return string
     */
    public static function joinPaths(string ...$paths): string
    {
        return self::normalizePath(implode(DIRECTORY_SEPARATOR, $paths));
    }

    /**
     * Fixes permissions to a specific file or directory. Directories can be fixed recursively.
     *
     * @param string $path
     * @param int $dirMode [OPTIONAL] Default value is 0777
     * @param int $fileMode [OPTIONAL] Default value is 0666
     * @return void
     * @throws IOException
     */
    public static function makeWritable(string $path, int $dirMode = 0777, int $fileMode = 0666): void
    {
        NetteFileSystem::makeWritable($path, $dirMode, $fileMode); // @phpstan-ignore-line
    }

    /**
     * Normalizes `..` and `.` and directory separators in path.
     *
     * @param string $path
     * @return string
     */
    public static function normalizePath(string $path): string
    {
        return NetteFileSystem::normalizePath($path); // @phpstan-ignore-line
    }

    /**
     * Reads the content of a file.
     *
     * @param string $file
     * @return string
     * @throws IOException
     */
    public static function read(string $file): string
    {
        return NetteFileSystem::read($file); // @phpstan-ignore-line
    }

    /**
     * Renames or moves a file or a directory. Overwrites existing files and directories by default.
     *
     * @param string $original
     * @param string $target
     * @param bool $overwrite [OPTIONAL] Default value is true
     * @return void
     * @throws IOException
     * @throws InvalidStateException If $overwrite is set to false and destination already exists
     */
    public static function rename(string $original, string $target, bool $overwrite = true): void
    {
        NetteFileSystem::rename($original, $target, $overwrite); // @phpstan-ignore-line
    }

    /**
     * Create file with unique file name
     *
     * @param string $directory
     * @param string $prefix
     * @param int|null $mode [OPTIONAL] Default value is 0666
     * @return string|false
     * @see https://www.php.net/manual/en/function.tempnam.php
     */
    public static function tempnam(string $directory, string $prefix, ?int $mode = 0666): string|false
    {
        $filePath = false;
        if (is_writable($directory) === true) {
            do {
                $randSuffix = Random::generate(6, '0-9a-zA-Z');
                $fileName = Strings::replace($prefix, '~(.*)(\.(?:.*))~', '$1' . '.' . $randSuffix . '$2');
                $filePath = $directory . DIRECTORY_SEPARATOR . $fileName;
            } while (file_exists($filePath) === true);
            self::write($filePath, '', $mode);
        }
        return $filePath;
    }

    /**
     * Writes the string to a file.
     *
     * @param string $file
     * @param string $content
     * @param int|null $mode [OPTIONAL] Default value is 0666
     * @return void
     * @throws IOException
     */
    public static function write(string $file, string $content, ?int $mode = 0666): void
    {
        NetteFileSystem::write($file, $content, $mode); // @phpstan-ignore-line
    }
}
