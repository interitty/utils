<?php

declare(strict_types=1);

namespace Interitty\Utils;

use ReflectionMethod;

abstract class BaseAttribute
{
    /** @var ReflectionMethod */
    protected ReflectionMethod $reflectionMethod;

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * ReflectionMethod getter
     *
     * @return ReflectionMethod
     */
    public function getReflectionMethod(): ReflectionMethod
    {
        return $this->reflectionMethod;
    }

    /**
     * ReflectionMethod setter
     *
     * @param ReflectionMethod $reflectionMethod
     * @return static Provides fluent interface
     */
    public function setReflectionMethod(ReflectionMethod $reflectionMethod): static
    {
        $this->reflectionMethod = $reflectionMethod;
        return $this;
    }

    // </editor-fold>
}
