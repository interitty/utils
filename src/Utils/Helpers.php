<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Interitty\Exceptions\Exceptions;
use Nette\Utils\Helpers as NetteHelpers;
use RuntimeException;

use function array_merge;
use function array_pop;
use function array_unique;
use function class_exists;
use function class_uses;
use function get_object_vars;
use function get_parent_class;
use function in_array;
use function is_a;
use function is_object;

class Helpers extends NetteHelpers
{
    /**
     * Class uses getter
     *
     * @param object|string $objectOrClass
     * @param bool $autoload [OPTIONAL]
     * @return array<string>
     * @phpstan-return array<class-string>
     */
    public static function getClassUses(object|string $objectOrClass, bool $autoload = true): array
    {
        $traits = [];
        do {
            $traits = array_merge((array) class_uses($objectOrClass, $autoload), $traits);
        } while ($objectOrClass = get_parent_class($objectOrClass));

        $traitsToSearch = $traits;
        while ($traitsToSearch !== []) {
            $newTraits = (array) class_uses((string) array_pop($traitsToSearch), $autoload);
            $traits = array_merge($newTraits, $traits);
            $traitsToSearch = array_merge($newTraits, $traitsToSearch);
        }
        /** @phpstan-var array<class-string> $traits */
        return array_unique($traits);
    }

    /**
     * Trait class used by class or object checker
     *
     * @param object|string $objectOrClass
     * @param string $taitClass
     * @return bool
     */
    public static function isTraitUsed(object|string $objectOrClass, string $taitClass): bool
    {
        $traitUsed = in_array($taitClass, self::getClassUses($objectOrClass), true);
        return $traitUsed;
    }

    /**
     * Class or object of type checker
     *
     * @param object|string $objectOrClass
     * @param string|string[] $class
     * @return bool
     */
    public static function isTypeOf(object|string $objectOrClass, array|string $class): bool
    {
        $typeOf = false;
        foreach ((array) $class as $oneClass) {
            if (is_a($objectOrClass, $oneClass, true) === true) {
                $typeOf = true;
                break;
            }
        }
        return $typeOf;
    }

    /**
     * Isolated require (class autoload) processor
     *
     * @param string $pluginFile
     * @return void
     * @throws RuntimeException
     */
    public static function processIsolatedRequire(string $className, string $pluginFile): void
    {
        $closure = static function (string $className, string $pluginFile): void {
            if (class_exists($className, false) === false) {
                require($pluginFile);
            }
        };
        $closure($className, $pluginFile);
        if (class_exists($className, false) !== true) {
            throw Exceptions::extend(RuntimeException::class)->setMessage('Class not found');
        }
    }

    /**
     * Multiple properties to the given object setter
     *
     * @param object $object
     * @param object|mixed[] $vars
     * @return void
     */
    public static function setObjectVars(object $object, $vars): void
    {
        if (is_object($vars) === true) {
            $vars = get_object_vars($vars);
        }
        foreach ($vars as $key => $value) {
            $object->{$key} = $value;
        }
    }
}
