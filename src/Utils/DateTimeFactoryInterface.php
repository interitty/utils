<?php

declare(strict_types=1);

namespace Interitty\Utils;

use DateTimeZone;
use Exception;

interface DateTimeFactoryInterface
{
    /**
     * DateTime factory
     *
     * @param string $time [OPTIONAL] A string representing the DateTime
     * @param string|DateTimeZone|null $timeZone [OPTIONAL] Desired Timezone
     * @return DateTime
     * @throws Exception
     */
    public function create(string $time = 'now', $timeZone = null): DateTime;

    /**
     * DateTime from format factory
     *
     * @param string $format Format of the $string parameter should be in
     * @param string $time A string representing the DateTime
     * @param string|DateTimeZone|null $timeZone [OPTIONAL] Desired Timezone
     * @return DateTime
     * @throws Exception
     */
    public function createFromFormat(string $format, string $time, $timeZone = null): DateTime;

    /**
     * DateTime from IsoDateTime format
     *
     * @param string $isoDateTime A string representing the DateTime in ISO 8601 format
     * @param string|DateTimeZone|null $timeZone [OPTIONAL] Desired Timezone
     * @return DateTime
     * @throws Exception
     */
    public function createFromIso8601(string $isoDateTime, $timeZone = null): DateTime;

    /**
     * DateTimeZone factory
     *
     * @param string|DateTimeZone|null $timeZone [OPTIONAL]
     * @return DateTimeZone
     * @throws Exception
     */
    public function createTimeZone($timeZone = null): DateTimeZone;
}
