<?php

declare(strict_types=1);

namespace Interitty\Exceptions;

use Exception;
use Interitty\Utils\ReflectionObject;
use Interitty\Utils\Strings;
use Nette\Localization\Translator;
use ReflectionClass;
use Stringable;
use Throwable;

use function array_shift;
use function is_scalar;

trait ExtendedExceptionTrait
{
    /** @var mixed[] */
    protected array $data = [];

    /** @var string */
    protected string $messageTemplate;

    /** @var Translator|null */
    protected ?Translator $translator = null;

    /**
     * Constructor
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     * @return void
     */
    public function __construct(string $message = '', int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->setData([]);
        $this->setMessage($message);
        $this->processFixCompatibility();
    }

    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Convert exception to string magic method
     *
     * @return string
     */
    public function __toString(): string
    {
        $message = $this->getMessageTemplate();
        if ($message !== '') {
            $data = $this->getData();
            $translator = $this->getTranslator();
            if ($translator instanceof Translator) {
                $message = $translator->translate($message);
            }
            foreach ($data as $key => $value) {
                if ((is_scalar($value) === true) || ($value instanceof Stringable)) {
                    $message = Strings::replace($message, '~:' . $key . '~', (string) $value);
                }
            }
        }
        return $message;
    }

    /**
     * Convert exception to string helper
     *
     * @return string
     */
    public function toString(): string
    {
        return (string) $this;
    }

    /**
     * Fix compatibility processor
     *
     * @return void
     */
    protected function processFixCompatibility(): void
    {
        $trace = $this->getTrace();
        array_shift($trace);

        ReflectionObject::setNonPublicPropertyValue($this, 'trace', $trace);
        ReflectionObject::setNonPublicPropertyValue($this, 'file', $trace[0]['file']);
        ReflectionObject::setNonPublicPropertyValue($this, 'line', $trace[0]['line']);
    }

    /**
     * Reload message processor
     *
     * @param bool $persistTemplate [OPTIONAL]
     * @return void
     */
    protected function processReloadParentMessage(bool $persistTemplate = false): void
    {
        $this->message = $persistTemplate ? $this->getMessageTemplate() : $this->__toString();
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Code getter
     *
     * @return int
     */
    abstract public function getCode();

    /**
     * Code setter
     *
     * @param int $code
     * @return static Provides fluent interface
     */
    public function setCode(int $code): static
    {
        $this->code = $code;
        $this->processReloadParentMessage();
        return $this;
    }

    /**
     * Data adder
     *
     * @param string $key
     * @param mixed $value
     * @return static Provides fluent interface
     */
    public function addData(string $key, mixed $value): static
    {
        $this->data[$key] = $value;
        $this->processReloadParentMessage();
        return $this;
    }

    /**
     * Data getter
     *
     * @return mixed[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Data setter
     *
     * @param mixed[] $data
     * @return static Provides fluent interface
     */
    public function setData(array $data): static
    {
        $this->data = [];
        foreach ($data as $key => $value) {
            $this->addData($key, $value);
        }
        return $this;
    }

    /**
     * Message getter
     *
     * @return string
     */
    abstract public function getMessage(): string;

    /**
     * Message adder
     *
     * @param string $message
     * @param bool $peristTemplate [OPTIONAL]
     * @return static Provides fluent interface
     */
    public function addMessage(string $message, bool $peristTemplate = false): static
    {
        $oldMessage = $this->getMessageTemplate();
        $this->setMessage($oldMessage . $message, $peristTemplate);
        return $this;
    }

    /**
     * Message setter
     *
     * @param string $message
     * @param bool $peristTemplate [OPTIONAL]
     * @return static Provides fluent interface
     */
    public function setMessage(string $message, bool $peristTemplate = false): static
    {
        $this->messageTemplate = $message;
        $this->processReloadParentMessage($peristTemplate);
        return $this;
    }

    /**
     * Message template getter
     *
     * @return string
     */
    public function getMessageTemplate(): string
    {
        return $this->messageTemplate;
    }

    /**
     * Previous getter
     *
     * @return Throwable|null
     */
    abstract public function getPrevious(): ?Throwable;

    /**
     * Previous setter
     *
     * @param Throwable $previous
     * @return static Provides fluent interface
     */
    public function setPrevious(Throwable $previous): static
    {
        $reflection = new ReflectionClass(Exception::class);
        $property = $reflection->getProperty('previous');
        $property->setAccessible(true);
        $property->setValue($this, $previous);
        return $this;
    }

    /**
     * Translator getter
     *
     * @return Translator|null
     */
    public function getTranslator(): ?Translator
    {
        return $this->translator;
    }

    /**
     * Translator setter
     *
     * @param Translator $translator
     * @return static Provides fluent interface
     */
    public function setTranslator(Translator $translator): static
    {
        $this->translator = $translator;
        $this->processReloadParentMessage();
        return $this;
    }

    // </editor-fold>
}
