<?php

namespace Interitty\Exceptions;

use Nette\Localization\Translator;
use Stringable;
use Throwable;

interface ExtendedExceptionInterface extends Stringable, Throwable
{
    /**
     * Convert exception to string helper
     *
     * @return string
     */
    public function toString(): string;

    /**
     * Code getter
     *
     * @return int
     */
    public function getCode();

    /**
     * Code setter
     *
     * @param int $code
     * @return static Provides fluent interface
     */
    public function setCode(int $code): static;

    /**
     * Data adder
     *
     * @param string $key
     * @param mixed $value
     * @return static Provides fluent interface
     */
    public function addData(string $key, mixed $value): static;

    /**
     * Data getter
     *
     * @return mixed[]
     */
    public function getData(): array;

    /**
     * Data setter
     *
     * @param mixed[] $data
     * @return static Provides fluent interface
     */
    public function setData(array $data): static;

    /**
     * Message adder
     *
     * @param string $message
     * @param bool $peristTemplate [OPTIONAL]
     * @return static Provides fluent interface
     */
    public function addMessage(string $message, bool $peristTemplate = false): static;

    /**
     * Message getter
     *
     * @return string
     */
    public function getMessage(): string;

    /**
     * Message setter
     *
     * @param string $message
     * @param bool $peristTemplate [OPTIONAL]
     * @return static Provides fluent interface
     */
    public function setMessage(string $message, bool $peristTemplate = false): static;

    /**
     * Message template getter
     *
     * @return string
     */
    public function getMessageTemplate(): string;

    /**
     * Previous getter
     *
     * @return Throwable|null
     */
    public function getPrevious(): ?Throwable;

    /**
     * Previous setter
     *
     * @param Throwable $previous
     * @return static Provides fluent interface
     */
    public function setPrevious(Throwable $previous): static;

    /**
     * Translator getter
     *
     * @return Translator|null
     */
    public function getTranslator(): ?Translator;

    /**
     * Translator setter
     *
     * @param Translator $translator
     * @return static Provides fluent interface
     */
    public function setTranslator(Translator $translator): static;
}
