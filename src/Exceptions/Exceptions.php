<?php

declare(strict_types=1);

namespace Interitty\Exceptions;

use Closure;
use Interitty\Utils\FileSystem;
use Interitty\Utils\Strings;
use Interitty\Utils\Validators;
use Nette\Localization\Translator;
use Nette\Utils\AssertionException;
use Throwable;

use function assert;
use function file_exists;
use function get_class;
use function is_a;
use function sys_get_temp_dir;

use const DIRECTORY_SEPARATOR;

class Exceptions
{
    /** @var string|null */
    protected static string|null $tempDir = null;

    /** @var Translator|null */
    protected static Translator|null $translator = null;

    /**
     * Extend given exception
     *
     * @param Throwable|string $exception
     * @return ExtendedExceptionInterface
     * @template ThrowableType of Throwable
     * @phpstan-param ThrowableType|class-string<ThrowableType> $exception
     * @phpstan-return ExtendedExceptionInterface&ThrowableType
     */
    public static function extend(Throwable|string $exception): ExtendedExceptionInterface
    {
        $message = '';
        $code = 0;
        $previous = null;
        if ($exception instanceof Throwable) {
            $message = $exception->getMessage();
            $code = $exception->getCode();
            $previous = $exception->getPrevious();
            $exception = get_class($exception);
        } elseif (is_a($exception, Throwable::class, true) !== true) {
            throw self::extend(AssertionException::class)
                    ->setMessage('Given exception should be Throwable object or class name');
        }

        $factory = self::createExceptionFactory($exception);
        /**
         * @var ExtendedExceptionInterface&Throwable $object
         * @phpstan-var ExtendedExceptionInterface&ThrowableType $object
         */
        $object = $factory($message, $code, $previous);
        $translator = self::getTranslator();
        if ($translator instanceof Translator) {
            $object->setTranslator($translator);
        }
        return $object;
    }

    /**
     * Exception factory generator
     *
     * @param string $class
     * @return Closure
     */
    protected static function createExceptionFactory(string $class): Closure
    {
        $tempDir = self::getTempDir();
        $fileName = $tempDir . DIRECTORY_SEPARATOR . Strings::replace($class, '~\\\\~', '-');
        if (file_exists($fileName) === false) {
            $factoryCode = '<?php
declare(strict_types=1);

use ' . ExtendedExceptionInterface::class . ';
use ' . ExtendedExceptionTrait::class . ';

return function(string $message = \'\', int $code = 0, ?Throwable $previous = null): ExtendedExceptionInterface {
    return new class($message, $code, $previous) extends ' . $class . ' implements ExtendedExceptionInterface {
        use ExtendedExceptionTrait;
    };
};';
            FileSystem::write($fileName, $factoryCode);
        }
        /** @phpstan-var Closure $factory */
        $factory = require $fileName;
        return $factory;
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * TempDir getter
     *
     * @return string
     * @internal
     */
    public static function getTempDir(): string
    {
        $tempDir = self::$tempDir;
        if ($tempDir === null) {
            $tempDir = sys_get_temp_dir();
            self::setTempDir($tempDir);
        }
        return $tempDir;
    }

    /**
     * TempDir setter
     *
     * @param string $tempDir
     * @return void
     */
    public static function setTempDir(string $tempDir): void
    {
        assert(Validators::check($tempDir, 'directory', 'tempDir'));
        assert(Validators::check($tempDir, 'writable', 'tempDir'));
        self::$tempDir = $tempDir;
    }

    /**
     * Translator getter
     *
     * @return Translator|null
     * @internal
     */
    public static function getTranslator(): ?Translator
    {
        return self::$translator;
    }

    /**
     * Translator setter
     *
     * @param Translator $translator
     * @return void
     */
    public static function setTranslator(Translator $translator): void
    {
        self::$translator = $translator;
    }

    // </editor-fold>
}
