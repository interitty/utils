<?php

declare(strict_types=1);

namespace Interitty\Exceptions;

use Closure;
use PHPUnit\Framework\Constraint\Constraint;
use PHPUnit\Framework\Constraint\Exception as ExceptionConstraint;
use Throwable;

use function assert;
use function call_user_func_array;

trait ExtendedExceptionPhpunitTrait
{
    /** @var Closure[] */
    protected array $expectedCallbacks = [];

    /** @var static|null */
    protected static $currentTestCase;

    /**
     * Thrown exception additional expectation
     *
     * @param Closure $callback
     * @return void
     */
    public function expectExceptionCallback(Closure $callback): void
    {
        self::$currentTestCase = $this;

        if ($this->getExpectedException() === null) {
            $this->expectException(Throwable::class);
        }
        $this->expectedCallbacks[] = $callback;
    }

    /**
     * Thrown exception contain the same data expectation
     *
     * @param array<string, mixed> $data
     * @return void
     */
    public function expectExceptionData(array $data = []): void
    {
        $this->expectExceptionCallback(static function (Throwable $exception) use ($data): void {
            assert($exception instanceof ExtendedExceptionInterface);
            self::assertSame($data, $exception->getData());
        });
    }

    /**
     * @inheritDoc
     */
    abstract public static function assertSame($expected, $actual, string $message = ''): void;

    /**
     * @inheritDoc
     * @param mixed $value
     */
    public static function assertThat($value, Constraint $constraint, string $message = ''): void
    {
        if ($value instanceof ExtendedExceptionInterface) {
            $value->setMessage($value->getMessageTemplate(), true);
        }
        parent::assertThat($value, $constraint, $message);
        $that = self::$currentTestCase;
        if (($constraint instanceof ExceptionConstraint) && ($value instanceof Throwable) && ($that instanceof self)) {
            $that->processAssertExceptionCallbacks($value);

            $newConstraint = new ExceptionConstraint((string) $that->getExpectedException());
            if ($constraint->toString() !== $newConstraint->toString()) {
                $newConstraint->evaluate($value, $message);
            }
        }
    }

    /**
     * @inheritDoc
     */
    protected function setUp(): void
    {
        self::$currentTestCase = null;
        $this->expectedCallbacks = [];
        parent::setUp();
    }

    /**
     * Assert exception against given callbacks processor
     *
     * @param Throwable $exception
     * @return void
     */
    protected function processAssertExceptionCallbacks(Throwable $exception): void
    {
        $expectedCallbacks = $this->getExpectedExceptionCallbacks();
        foreach ($expectedCallbacks as $expectedCallback) {
            call_user_func_array($expectedCallback, [$exception]);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Expected exception type getter
     *
     * @return string|null
     */
    abstract public function getExpectedException(): ?string;

    /**
     * Setup expected exception type check
     *
     * @param string $exception
     * @return void
     * @phpstan-param class-string<Throwable> $exception
     */
    abstract public function expectException(string $exception): void;

    /**
     * Expected exception callbacks getter
     *
     * @return Closure[]
     */
    public function getExpectedExceptionCallbacks(): array
    {
        return $this->expectedCallbacks;
    }

    // </editor-fold>
}
