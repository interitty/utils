<?php

declare(strict_types=1);

namespace Interitty\Iterators;

use Countable;
use Interitty\Exceptions\Exceptions;
use Interitty\Utils\Validators;
use Iterator;
use Nette\Utils\AssertionException;
use Traversable;

use function array_key_exists;
use function assert;
use function count;
use function current;
use function key;
use function next;
use function reset;

/**
 * @template CollectionValue
 * @implements Iterator<int|string, CollectionValue>
 */
class Collection implements Iterator, Countable
{
    /** @var array<int|string, CollectionValue> All values in set */
    protected array $data = [];

    /** @var string Allowed types of values */
    protected string $type;

    /**
     * Constructor
     *
     * @param string $type
     * @return void
     */
    public function __construct(string $type)
    {
        $this->setType($type);
    }

    /**
     * Add item to collection data
     *
     * @param mixed $item
     * @param int|string|null $key [OPTIONAL]
     * @return static Provides fluent interface
     * @phpstan-param CollectionValue $item
     */
    public function add($item, $key = null)
    {
        $this->checkType($item);
        $this->data[$key] = $item;
        return $this;
    }

    /**
     * Add all collection items into collection data
     *
     * @param array|Traversable $collection
     * @return static Provides fluent interface
     * @phpstan-param array<int|string, CollectionValue>|Traversable<int|string, CollectionValue> $collection
     */
    public function addCollection($collection)
    {
        Validators::is($collection, 'iterable');
        foreach ($collection as $key => $item) {
            $this->add($item, $key);
        }
        return $this;
    }

    /**
     * Count items
     *
     * @return int
     * @phpstan-return int<0, max>
     */
    public function count(): int
    {
        return count($this->data);
    }

    /**
     * Get current item
     *
     * @return mixed
     * @phpstan-return CollectionValue
     */
    public function current(): mixed
    {
        $key = $this->key();
        $current = $this->get($key);
        return $current;
    }

    /**
     * Delete item by given key from set
     *
     * @param int|string $key
     * @return bool
     */
    public function delete($key): bool
    {
        $result = false;
        if ($this->has($key) === true) {
            unset($this->data[$key]);
            $result = true;
        }
        return $result;
    }

    /**
     * Get item by given key
     *
     * @param int|string $key
     * @return mixed
     * @phpstan-return CollectionValue
     */
    public function get($key)
    {
        if ($this->has($key) !== true) {
            throw Exceptions::extend(AssertionException::class)
                    ->setMessage('Item with key ":key" was not added')
                    ->addData('key', $key);
        }
        $item = $this->data[$key];
        return $item;
    }

    /**
     * Get current key
     *
     * @return int|string
     */
    public function key(): mixed
    {
        $key = key($this->data);
        assert($key !== null);
        return $key;
    }

    /**
     * Get next item
     *
     * @return void
     */
    public function next(): void
    {
        next($this->data);
    }

    /**
     * Rewind
     *
     * @return void
     */
    public function rewind(): void
    {
        reset($this->data);
    }

    /**
     * Check is there are more items
     *
     * @return bool
     */
    public function valid(): bool
    {
        return current($this->data) !== false;
    }

    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Item checker
     *
     * @param int|string $key
     * @return bool
     */
    protected function has($key): bool
    {
        $has = array_key_exists($key, $this->data);
        return $has;
    }

    /**
     * Type checker
     *
     * @param mixed $value
     * @return void
     */
    protected function checkType($value): void
    {
        $type = $this->getType();
        Validators::assert($value, $type);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Return allowed type
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Type setter
     *
     * @param string $type
     * @return static Provides fluent interface
     */
    protected function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }

    // </editor-fold>
}
