<?php

declare(strict_types=1);

namespace Interitty\Iterators;

use Nette\Iterators\CachingIterator as NetteCachingIterator;

class CachingIterator extends NetteCachingIterator
{
}
